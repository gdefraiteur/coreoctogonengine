﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Dynamic;

namespace OctogonEngine
{
	[System.Serializable]
	public class MonoBehaviour {
		private static int									_index;
		private int											_id;
		private List<MonoBehaviour>							_components;
		public List<MonoBehaviour>							components
		{
			
			get
			{
				return ((List<MonoBehaviour>)this._components.Clone());
			}
			private set
			{
				this._components = value;
			}
		}
		private List<UserScript>							userScripts = new List<UserScript>();
		private UserScript									script = null;
		public bool											hasScript(String scriptName)
		{
			for (int i = 0; i < userScripts.Count; i++)
			{
				if (userScripts[i].className == scriptName)
				{
					return (true);
				}
			}
			return (false);
		}
		public static readonly List<MonoBehaviour>			MonoBehaviours = new List<MonoBehaviour>();
		public virtual int											id
		{
			get 
			{
				return (this._id);
			}
			set
			{
				;
			}
		}
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast to Point return false.
			MonoBehaviour p = obj as MonoBehaviour;
			if ((System.Object)p == null)
			{
				return false;
			}

			// Return true if the fields match:
			return (p.GetHashCode() == this.GetHashCode());
		}
		public bool Equals(MonoBehaviour other)
		{
			if ((object)other == null)
	        {
	            return false;
	        }

	        // Return true if the fields match:
	        return (other.GetHashCode() == this.GetHashCode());
		}
		public override int										GetHashCode()
		{
			return (this._id);
		}

		public override string							ToString()
		{
			return ("MonoBehaviour :\n nb : " + this.GetType() + ";");
		}
		//public static MonoBehaviour							(componentTable[])(string value);
		//public static int										nbAvaillable;
		public Transform								transform
		{
			get
			{
				return (this.getComponent<Transform>());
			}
		}
		public MonoBehaviour							gameObject;
		public MonoBehaviour							mesh
		{
			get
			{
				return (this.getComponent<Mesh>());
			}
		}

		public static bool		operator==(MonoBehaviour self, MonoBehaviour other)
		{
			if ((object)other == null && (object)self == null)
				return (true);
			if ((object)other == null)
				return (false);
			if (self.GetHashCode() == other.GetHashCode())
			{
				return (true);
			}
			return (false);
		}
		public static bool operator !=(MonoBehaviour self, MonoBehaviour other)
		{
			return (!(self == other));
		}
		public virtual void	copyStandards(MonoBehaviour other)
		{
			this.gameObject = other.gameObject;
			//this.mesh = other.mesh;
		}
		/// ca va pas faut l enlever de tt les autres aussi.
		

		public virtual void unloadScripts()
		{
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				UserScript script = this.userScripts[i];
				this.removeComponents(script.type, true);
			}
		}

		public virtual void unloadScripts(UserScript script)
		{
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				if (script == this.userScripts[i])
				{
					//UserScript script = this.userScripts[i];
					this.removeComponents(script.type, true);
				}
			}
		}

		private CustomProperty _customProperty = null;
		public virtual CustomProperty customProperty
		{
			get
			{
				if (this._customProperty == null)
				{
					Debug.Log("NULL");
					this._customProperty = new CustomProperty(null, this.transform.name, this);
				}
				return (this._customProperty);
			}
			set
			{
				if (this._customProperty == null)
				{
					Debug.Log("NULL");
					this._customProperty = new CustomProperty(null, this.transform.name, this);
				}
				this._customProperty = value;
			}
		}
		public virtual dynamic AddComponent(Type type)
		{
			UserScript script = null;
			if (UserScript.Contains(type.Name))
			{
				Debug.Log("script found");
				script = UserScript.Find(type.Name);
				this.userScripts.Add(script);
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i].userScripts.Add(script);
				}
			}
			MonoBehaviour self = this;

			if ((type).IsSubclassOf(typeof(MonoBehaviour)))
			{
				dynamic tmp = Activator.CreateInstance(type);
				if (script != null)
				{
					Debug.Log("setted script to script");
					((MonoBehaviour)tmp).script = script;
				}
				((MonoBehaviour)tmp).copyStandards(this);
				if (this._components == null)
				{
					this._components = new List<MonoBehaviour>();
				}
				this._components.Add((MonoBehaviour)tmp);
				//((MonoBehaviour)tmp)._components = ((MonoBehaviour)this)._components;
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i]._components = this._components;
				}
				return (tmp);
			}
			else
			{
				Debug.Log("wrong type T.T");
			}
			return (null);
		}

		public virtual void reloadScripts()
		{
			this.unloadScripts();
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				UserScript script = this.userScripts[i];
				if (script.Exist())
				{
					if (script.type.IsSubclassOf(typeof(MonoBehaviour)))
					{
						AddComponent(script.type);
					}
				}
			}
		}

		public virtual void reloadScripts(UserScript script)
		{

			this.unloadScripts(script);
			Debug.Log("i have:" + this.userScripts.Count +" scripts");
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				if (this.userScripts[i].className == script.className)
				{
					if (script.Exist())
					{
						Console.WriteLine("j'existe");
						if (script.type.IsSubclassOf(typeof(MonoBehaviour)))
						{
							AddComponent(script.type);
						}
					}
					Console.WriteLine("trouvé");
				}
			}
			Console.WriteLine("non");
		}

		public virtual T addComponent<T>() where T : new()
		{
			bool check = false;
			UserScript script = null;
			if (UserScript.Contains(typeof(T).Name))
			{
				script = UserScript.Find(typeof(T).Name);
				this.userScripts.Add(script);
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i].userScripts.Add(script);
				}
				check = true;
			}
			MonoBehaviour self = this;
			if ((typeof(T)).IsSubclassOf(typeof(MonoBehaviour)))
			{
				object tmp = new T();
				if (check)
					((MonoBehaviour)tmp).script = script;
				((MonoBehaviour)tmp).copyStandards(this);
				if (this._components == null)
				{
					this._components = new List<MonoBehaviour>();
				}
				this._components.Add((MonoBehaviour)tmp);
				((MonoBehaviour)tmp)._components = ((MonoBehaviour)this)._components;
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i]._components = this._components;
				}
				return ((T)tmp);
			}
			return (default(T));
		}

		public virtual T		getComponent<T>() where T : new()
		{
			if ((typeof(T)).IsSubclassOf(typeof(MonoBehaviour)))
			{
				for (int i = 0; i < this._components.Count; i++)
				{
					if (this._components[i].GetType() == typeof(T))
					{
						object tmp = this._components[i];
						return ((T)tmp);
					}
				}
			}
			return (default(T));
		}

		public virtual List<T>		getComponents<T>() where T : new()
		{
			List<T> res = new List<T>();
			if ((typeof(T)).IsSubclassOf(typeof(MonoBehaviour)))
			{
				for (int i = 0; i < this._components.Count; i++)
				{
					if (this._components[i].GetType() == typeof(T))
					{
						object tmp = this._components[i];
						res.Add((T)tmp);
					}
				}
			}
			return (res);
		}

		public MonoBehaviour()
		{
			//this.mesh = null;
			//this.transform = null;
			
			this.gameObject = null;
			this._components = new List<MonoBehaviour>();
			
			this._id = MonoBehaviour._index;
			MonoBehaviour._index += 1;
			Type t = this.GetType();
			if (UserScript.Contains(t.Name))
			{
				this.script = UserScript.Find(t.Name);
			}
			this._components.Add(this);
			MonoBehaviour.MonoBehaviours.Add(this);
			//MonoBehaviour tmp = this.addComponent< >();
			//tmp = this;
		}

			
		public virtual void			removeComponent<T>() where T : new()
		{
			if ((typeof(T)).IsSubclassOf(typeof(MonoBehaviour)))
			{
				List< MonoBehaviour > newComponents = new List< MonoBehaviour >();
				List< UserScript > newUserScripts = new List< UserScript >();
				for (int i = 0; i < this._components.Count; i++)
				{
					if ((this._components[i].GetType() == typeof(T)))
					{
						;
					}
					else
					{
						if (this._components[i].script != null)
						{
							newUserScripts.Add(this._components[i].script);
						}
						newComponents.Add(((this._components)[i]));
					}
				}
				List<MonoBehaviour > tmp = _components;
				_components = newComponents;
				userScripts = newUserScripts;
				for (int i = 0; i < this._components.Count; i++)
				{
					((this._components)[i])._components = this._components;
					this._components[i].userScripts = this.userScripts;
				}
			}
		}
		public virtual List<MonoBehaviour> getAllComponents()
		{
			//List<MonoBehaviour> res = new List<MonoBehaviour>();
			return ((List<MonoBehaviour>)this._components.Clone());

		}
		public virtual void removeComponents(Type type, bool keepScript)
		{
			List<MonoBehaviour> newComponents = new List<MonoBehaviour>();
			List<UserScript> newUserScripts = new List<UserScript>();
			for (int i = 0; i < this._components.Count; i++)
			{
				if ((this._components[i].GetType() == type))
				{
					if (keepScript && this._components[i].script != null)
					{
						Debug.Log("keeped script");
						newUserScripts.Add(this._components[i].script);
						Debug.Log("newUserScripts:" + newUserScripts.Count);
					}
					MonoBehaviour.MonoBehaviours[this._components[i].GetHashCode()] = null;
				}
				else
				{
					if (this._components[i].script != null)
					{
						newUserScripts.Add(this._components[i].script);
					}
					newComponents.Add(((this._components)[i]));
				}
			}
			Debug.Log("MAIS???:" + newUserScripts.Count);
			List<MonoBehaviour> tmp = _components;
			_components = newComponents;
			userScripts = newUserScripts;
			for (int i = 0; i < this._components.Count; i++)
			{
				((this._components)[i])._components = this._components;
				this._components[i].userScripts = this.userScripts;
			}
			Debug.Log("i have:" + userScripts.Count + " scripts");
		}

		public virtual void removeComponents(Type type)
		{
			List<MonoBehaviour> newComponents = new List<MonoBehaviour>();
			List<UserScript> newUserScripts = new List<UserScript>();
			for (int i = 0; i < this._components.Count; i++)
			{
				if ((this._components[i].GetType() == type))
				{
					MonoBehaviour.MonoBehaviours[this._components[i].GetHashCode()] = null;
				}
				else
				{
					if (this._components[i].script != null)
					{
						newUserScripts.Add(this._components[i].script);
					}
					newComponents.Add(((this._components)[i]));
				}
			}
			List<MonoBehaviour> tmp = _components;
			_components = newComponents;
			userScripts = newUserScripts;
			for (int i = 0; i < this._components.Count; i++)
			{
				((this._components)[i])._components = this._components;
				this._components[i].userScripts = this.userScripts;
			}
		}
		public virtual void			Update()
		{
			;
		}
		public virtual void			Start()
		{
			;
		}
	};
	
	
	
}
