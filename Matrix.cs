﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OctogonEngine;
using OpenTK;
using OpenTK.Graphics;
using GL = OpenTK.Graphics.OpenGL.GL;

/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

namespace OctogonEngine
{
	public struct Matrix
	{
		public static Matrix Zero
		{
			get
			{
				float[] array = new float[4 * 4];
				for (int i = 0; i < 16; i++)
				{
					array[i] = 0;
				}
				Matrix res = new Matrix(4, 4);
				res.array = array;
				return (res);
			}
		}
		public float[] array
		{
			get
			{
				return ((float[])this._array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		public override bool Equals(object obj)
    	{
    	    Matrix matrix = (Matrix)obj;
    	    if ((object)obj == null)
    	    {
    	    	return (false);
    	    }
    	    else
	    	    return matrix.GetHashCode() == this.GetHashCode();
    	}
    	public override int GetHashCode()
    	{
    		float hash = 0;
    		for (int i = 0; i < this.height; i++)
    		{
    			for (int j = 0; j < this.width; j++)
    			{
    				hash += (this.height + 1) * this[i, j];
    				hash += (i * j * 2); 
    			}
    		}
    		return ((int)hash);
    	}
		public static Matrix Orthographic(float left, float bottom, float right, float top, float near, float far)
		{

			Matrix res = Matrix.Zero;
			res.setRow(0, new float[]{(2f) / (right-left) , 0 , 0 , -(right+left) / (right-left)});
			res.setRow(1, new float[]{0 , (2f) / top-bottom , 0 , -(top+bottom) / (top-bottom)});
			res.setRow(2, new float[]{0 , 0 , (-2f) / far-near , (far+near) / (far-near)});
			res.setRow(3, new float[]{0 , 0 , 0 , 1});
			return (res);
		}
		public Matrix getInverse2(Matrix mat)
		{
			Vector3 _localPosition;
			Quaternion _localRotation;
			Vector3 _localScale;

			this.Decompose(out _localScale, out _localRotation, out _localPosition);

			Matrix translation = Matrix.translation(Vector3.zero - _localPosition);
			Matrix rotation = _localRotation.inverse.orthogonalMatrix;
			Matrix scale = Matrix.scale(new Vector3(1 / _localScale.x, 1 / _localScale.y, 1 / _localScale.z));
			return (translation * rotation * scale);
		}
		private float[] _array;
		public int width;
		public int height;
		public static float square(float nb)
		{
			float res = nb * nb;
			return (res);
		}
		public static float to_rad(float angle)
		{
			return (angle * (float)Math.PI / 180f);
		}
		public static float abs(float value)
		{
			float res = value < 0 ? -value : value;
			return (res);
		}
		public Matrix(Vector3 vector)
		{
			this._array = new float[4];
			this.width = 1;
			this.height = 4;
			this[0, 0] = vector.x;
			this[1, 0] = vector.y;
			this[2, 0] = vector.z;
			this[3, 0] = 1;
		}

		public Matrix(Quaternion rotation)
		{
			Matrix tmp = rotation.orthogonalMatrix;
			this._array = tmp.array;
			this.width = 4;
			this.height = 4;
		}

		public Quaternion toQuaternion()
		{
			float trace = this[0] + this[5] + this[10];
			float qw = 1;
			float qx = 0;
			float qy = 0;
			float qz = 0;
			if (trace > 0)
			{
				float S = (float)Mathf.Sqrt(trace + 1.0) * 2; // S=4*qw 
  				if (S == 0)
  					return (new Quaternion(qx, qy, qz, qw));
  				qw = 0.25f * S;
  				qx = (this[2, 1] - this[1, 2]) / S;
  				qy = (this[0, 2] - this[2, 0]) / S; 
  				qz = (this[1, 0] - this[0, 1]) / S; 
				return (new Quaternion(qx, qy, qz, qw));
			}
			else if ((this[0, 0] > this[1, 1]) & (this[0, 0] > this[2, 2])) { 
				float S = (float)Mathf.Sqrt(1.0 + this[0, 0] - this[1, 1] - this[2, 2]) * 2; // S=4*qx 
				if (S == 0)
					return (new Quaternion(qx, qy, qz, qw));
				qw = (this[2, 1] - this[1, 2]) / S;
				qx = 0.25f * S;
				qy = (this[0, 1] + this[1, 0]) / S; 
				qz = (this[0, 2] + this[2, 0]) / S; 
			} else if (this[1, 1] > this[2, 2]) { 
				float S = (float)Mathf.Sqrt(1.0 + this[1, 1] - this[0, 0] - this[2, 2]) * 2; // S=4*qy
				if (S == 0)
					return (new Quaternion(qx, qy, qz, qw));
				qw = (this[0, 2] - this[2, 0]) / S;
				qx = (this[0, 1] + this[1, 0]) / S; 
				qy = 0.25f * S;
				qz = (this[1, 2] + this[2, 1]) / S; 
			} else { 
				float S = (float)Mathf.Sqrt(1.0 + this[2, 2] - this[0, 0] - this[1, 1]) * 2; // S=4*qz
				if (S == 0)
					return (new Quaternion(qx, qy, qz, qw));
				qw = (this[1, 0] - this[0, 1]) / S;
				qx = (this[0, 2] + this[2, 0]) / S;
				qy = (this[1, 2] + this[2, 1]) / S;
				qz = 0.25f * S;
			}
			return (new Quaternion(qx, qy, qz, qw));
		}

		public Quaternion quaternion
		{
			get
			{
				return (this.toQuaternion());
			}
			set
			{
				Matrix tmp = new Matrix(value);
				this._array = tmp.array;
				this.width = tmp.width;
				this.height = tmp.height;
			}
		}

		public static Quaternion operator *(Matrix self, Quaternion multiplied)
		{
			Matrix tmp = new Matrix(multiplied);
			tmp = self * tmp;
			//Vector3 res = new Vector3(tmp[0], tmp[1], tmp[2]);
			return (tmp.quaternion);
		}
		
		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public float this[int y, int x]
		{
			get
			{
				return (this._array[(y * this.width) + x]);
			}
			set
			{
				this._array[(y * this.width) + x] = value;
			}
		}
		public static Matrix XYZEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.x, eulerAngles.y, eulerAngles.z));
		}

		public static Matrix YXZEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.y, eulerAngles.x, eulerAngles.z));
		}

		public static Matrix ZXYEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.z, eulerAngles.x, eulerAngles.y));
		}

		public static Matrix ZYXEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.z, eulerAngles.y, eulerAngles.x));
		}

		public static Matrix YZXEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.y, eulerAngles.z, eulerAngles.x));
		}

		public static Matrix XZYEuler(Vector3 eulerAngles)
		{
			return (YPR(eulerAngles.x, eulerAngles.z, eulerAngles.y));
		}

		public static Matrix translation(Vector3 translationVector)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{1.0f, 0.0f, 0.0f, translationVector.x,
				0.0f, 1.0f, 0.0f, translationVector.y,
				0.0f, 0.0f, 1.0f, translationVector.z,
				0.0f, 0.0f, 0.0f, 1.0f};
			return (res);
		}
		public static Matrix translation(float x, float y, float z)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{1.0f, 0.0f, 0.0f, x,
				0.0f, 1.0f, 0.0f, y,
				0.0f, 0.0f, 1.0f, z,
				0.0f, 0.0f, 0.0f, 1.0f};
			return (res);
		}
		public static Matrix scale(Vector3 scaleVector)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{scaleVector.x, 0.0f, 0.0f, 0.0f,
				0.0f, scaleVector.y, 0.0f, 0.0f,
				0.0f, 0.0f, scaleVector.z, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f};
			return (res);
		}
		public static Matrix scale(float x, float y, float z)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{x, 0.0f, 0.0f, 0.0f,
				0.0f, y, 0.0f, 0.0f,
				0.0f, 0.0f, z, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f};
			return (res);
		}

		public static Matrix YPR(Vector3 eulerRepresentation)
		{
			float ch = (float)Math.Cos(to_rad(eulerRepresentation.x));
			float sh = (float)Math.Sin(to_rad(eulerRepresentation.x));
			float ca = (float)Math.Cos(to_rad(eulerRepresentation.y));
			float sa = (float)Math.Sin(to_rad(eulerRepresentation.y));
			float cb = (float)Math.Cos(to_rad(eulerRepresentation.z));
			float sb = (float)Math.Sin(to_rad(eulerRepresentation.z));
			Matrix res = Matrix.Identity;
			res[0, 0] = ch * ca;
			res[0, 1] = sh * sb - ch * sa * cb;
			res[0, 2] = ch * sa * sb + sh * cb;
			res[1, 0] = sa;
			res[1, 1] = ca * cb;
			res[1, 2] = -ca * sb;
			res[2, 0] = -sh * ca;
			res[2, 1] = sh * sa * cb + ch * sb;
			res[2, 2] = -sh * sa * sb + ch * cb;
			return (res);
		}
		public static Matrix YPR(float yaw, float pitch, float roll)
		{
			float ch = (float)Math.Cos(to_rad(yaw));
			float sh = (float)Math.Sin(to_rad(yaw));
			float ca = (float)Math.Cos(to_rad(pitch));
			float sa = (float)Math.Sin(to_rad(pitch));
			float cb = (float)Math.Cos(to_rad(roll));
			float sb = (float)Math.Sin(to_rad(roll));
			Matrix res = Matrix.Identity;
			res[0, 0] = ch * ca;
			res[0, 1] = sh * sb - ch * sa * cb;
			res[0, 2] = ch * sa * sb + sh * cb;
			res[1, 0] = sa;
			res[1, 1] = ca * cb;
			res[1, 2] = -ca * sb;
			res[2, 0] = -sh * ca;
			res[2, 1] = sh * sa * cb + ch * sb;
			res[2, 2] = -sh * sa * sb + ch * cb;
			return (res);
		}
		public static Matrix Identity
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				res.array = new float[]{1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f};
				return (res);
			}
		}
		public override string ToString()
		{
			string res = "Matrix(" + this.height + ", " + this.width + "): \n";
			for (int i = 0; i < this.height; i++)
			{
				res += "\t\t\t{ ";
				for (int j = 0; j < this.width; j++)
				{
					res += this[i, j];
					if ((j + 1) < this.width)
						res += ", ";
				}
				res += "} \n";

			}
			return res;
		}

		public Matrix(int width, int height)
		{
			this._array = new float[height * width];
			this.height = height;
			this.width = width;
			for (int i = 0; i < this.height; i++)
			{
				for (int j = 0; j < this.width; j++)
				{
					this[i, j] = 0.0f;
				}
			}
		}
		public static Vector3 operator *(Matrix self, Vector3 multiplied)
		{
			Matrix tmp = new Matrix(multiplied);
			//MessageBox.Show("multiplied :" + tmp);
			//MessageBox.Show("multiplier :" + self);
			//tmp = multiply(tmp, self);
			Vector3 res = new Vector3((float)((double)self[0, 0] * (double)multiplied[0] + (double)self[0, 1] * (double)multiplied[1] + (double)self[0, 2] * (double)multiplied[2] + (double)self[0, 3]),
									(float)((double)self[1, 0] * (double)multiplied[0] + (double)self[1, 1] * (double)multiplied[1] + (double)self[1, 2] * (double)multiplied[2] + (double)self[1, 3]),
									(float)((double)self[2, 0] * (double)multiplied[0] + (double)self[2, 1] * (double)multiplied[1] + (double)self[2, 2] * (double)multiplied[2] + (double)self[2, 3]));
			//Debug.Log("res:" + res);
			//MessageBox.Show("result :" + tmp);
			return (res);
		}

		public static Matrix operator *(Matrix multiplier, Matrix multiplied)
		{
			Matrix test3 = multiply2(multiplier, multiplied);
			return (test3);
		}

		public static Matrix multiply(Matrix multiplier, Matrix multiplied)
		{
			if ((multiplied).height == multiplier.width)
			{
				Matrix res = new Matrix(multiplied.width, (multiplier).height);
				for (int k = 0; k < multiplied.width; k++)
				{
					for (int i = 0; i < multiplier.height; i++)
					{
						for (int j = 0; j < (multiplied).height; j++)
						{
							(res)[k, j] += (multiplier)[k, i] * (multiplied)[i, j];
						}
					}
				}
				return (res);
			}
			return (new Matrix(4, 4));
		}

		public static Vector3 multiply(Matrix self, Vector3 vector)
		{
			return (self * vector);
		}
		public static Matrix multiply2(Matrix multiplied, Matrix multiplier)
		{
			if ((multiplied).width == multiplier.height)
			{
				Matrix res = new Matrix(multiplied.height, (multiplied).width);
				for (int k = 0; k < multiplied.height; k++)
				{
					for (int i = 0; i < multiplier.height; i++)
					{
						for (int j = 0; j < (multiplied).width; j++)
						{
							(res)[k, j] += (multiplier)[i, j] * (multiplied)[k, i];
						}
					}
				}
				return (res);
			}
			return (new Matrix(4, 4));
		}

		public float Determinant()
		{
			return this[0, 1 - 1] * this[1, 2 - 1] * this[2, 3 - 1] * this[3, 4 - 1] - this[0, 1 - 1] * this[1, 2 - 1] * this[2, 4 - 1] * this[3, 3 - 1] + this[0, 1 - 1] * this[1, 3 - 1] * this[2, 4 - 1] * this[3, 2 - 1] - this[0, 1 - 1] * this[1, 3 - 1] * this[2, 2 - 1] * this[3, 4 - 1]
				+ this[0, 1 - 1] * this[1, 4 - 1] * this[2, 2 - 1] * this[3, 3 - 1] - this[0, 1 - 1] * this[1, 4 - 1] * this[2, 3 - 1] * this[3, 2 - 1] - this[0, 2 - 1] * this[1, 3 - 1] * this[2, 4 - 1] * this[3, 1 - 1] + this[0, 2 - 1] * this[1, 3 - 1] * this[2, 1 - 1] * this[3, 4 - 1]
				- this[0, 2 - 1] * this[1, 4 - 1] * this[2, 1 - 1] * this[3, 3 - 1] + this[0, 2 - 1] * this[1, 4 - 1] * this[2, 3 - 1] * this[3, 1 - 1] - this[0, 2 - 1] * this[1, 1 - 1] * this[2, 3 - 1] * this[3, 4 - 1] + this[0, 2 - 1] * this[1, 1 - 1] * this[2, 4 - 1] * this[3, 3 - 1]
				+ this[0, 3 - 1] * this[1, 4 - 1] * this[2, 1 - 1] * this[3, 2 - 1] - this[0, 3 - 1] * this[1, 4 - 1] * this[2, 2 - 1] * this[3, 1 - 1] + this[0, 3 - 1] * this[1, 1 - 1] * this[2, 2 - 1] * this[3, 4 - 1] - this[0, 3 - 1] * this[1, 1 - 1] * this[2, 4 - 1] * this[3, 2 - 1]
				+ this[0, 3 - 1] * this[1, 2 - 1] * this[2, 4 - 1] * this[3, 1 - 1] - this[0, 3 - 1] * this[1, 2 - 1] * this[2, 1 - 1] * this[3, 4 - 1] - this[0, 4 - 1] * this[1, 1 - 1] * this[2, 2 - 1] * this[3, 3 - 1] + this[0, 4 - 1] * this[1, 1 - 1] * this[2, 3 - 1] * this[3, 2 - 1]
				- this[0, 4 - 1] * this[1, 2 - 1] * this[2, 3 - 1] * this[3, 1 - 1] + this[0, 4 - 1] * this[1, 2 - 1] * this[2, 1 - 1] * this[3, 3 - 1] - this[0, 4 - 1] * this[1, 3 - 1] * this[2, 1 - 1] * this[3, 2 - 1] + this[0, 4 - 1] * this[1, 3 - 1] * this[2, 2 - 1] * this[3, 1 - 1];
		}

		public void Decompose(out Vector3 scaling, out Quaternion rotation, out Vector3 translation)
		{
			//Extract the translation
			translation = new Vector3(0, 0, 0);
			translation.X = this[0, 4 - 1];
			translation.Y = this[1, 4 - 1];
			translation.Z = this[2, 4 - 1];
			scaling = new Vector3(0, 0, 0);
			rotation = Quaternion.identity;

			//Extract row vectors of the matrix
			Vector3 row1 = new Vector3(this[0, 1 - 1], this[0, 2 - 1], this[0, 3 - 1]);
			Vector3 row2 = new Vector3(this[1, 1 - 1], this[1, 2 - 1], this[1, 3 - 1]);
			Vector3 row3 = new Vector3(this[2, 1 - 1], this[2, 2 - 1], this[2, 3 - 1]);
			//Extract the scaling factors
			scaling.X = row1.Length();
			scaling.Y = row2.Length();
			scaling.Z = row3.Length();
			//Handle negative scaling
			if (Determinant() < 0)
			{
				scaling.X = -scaling.X;
				scaling.Y = -scaling.Y;
				scaling.Z = -scaling.Z;
			}
			//Remove scaling from the matrix
			if (scaling.X != 0)
			{
				row1 /= scaling.X;
			}

			if (scaling.Y != 0)
			{
				row2 /= scaling.Y;
			}

			if (scaling.Z != 0)
			{
				row3 /= scaling.Z;
			}


			//Build 3x3 rot matrix, convert it to quaternion
			Matrix rotMat = Matrix.Identity;
			
			rotMat.setRow(0, new float[]{row1.X, row1.Y, row1.Z, 0});
			rotMat.setRow(1, new float[]{row2.X, row2.Y, row2.Z, 0});
			rotMat.setRow(2, new float[]{row3.X, row3.Y, row3.Z, 0});

			rotation = new Quaternion(rotMat);
		}

		public Matrix(float[] mat)
		{
			this._array = (float[])(mat.Clone());
			this.height = (int)Math.Sqrt(mat.Length);
			this.width = (int)Math.Sqrt(mat.Length);
		}
		public void setRow(int index, float[] row)
		{
			for (int i = 0; i < this.width; i++)
			{
				this[index, i] = row[i];
			}
		}

		public void setRow(int index, double[] row)
		{
			for (int i = 0; i < this.width; i++)
			{
				this[index, i] = (float)row[i];
			}
		}

		public void setCol(int index, float[] col)
		{
			for (int i = 0; i < this.height; i++)
			{
				this[i, index] = col[i];
			}
		}

		public void setCol(int index, double[] col)
		{
			for (int i = 0; i < this.height; i++)
			{
				this[i, index] = (float)col[i];
			}
		}

		public float[] getCol(int index)
		{
			float[] res = new float[this.height];
			for (int i = 0; i < this.height; i++)
			{
				res[i] = this[i, index];
			}
			return (res);
		}

		public float[] getRow(int index)
		{
			float[] res = new float[this.width];
			for (int i = 0; i < this.width; i++)
			{
				res[i] = this[index, i];
			}
			return (res);
		}

		public static Matrix M4x4(Matrix matrix)
		{
			Matrix res = new Matrix(4, 4);
			res = matrix;
			if (matrix.height == 3 && matrix.width == 3)
			{
				for (int i = 0; i < 3; i++)
				{
					res[i, 3] = 0;
				}
				res.setRow(3, new float[] { 0.0f, 0.0f, 0.0f, 1.0f });
			}
			return (res);
		}


		public static void m3_inverse(ref Matrix mr, ref Matrix ma)
		{
			float det = m3_det(ref ma);
			if (Math.Abs(det) < 0.0005)
			{
				Console.WriteLine("Matrix determinant deduction failed");
				return;
			}
			mr[0] = ma[4] * ma[8] - ma[5] * ma[7] / det;
			mr[1] = -(ma[1] * ma[8] - ma[7] * ma[2]) / det;
			mr[2] = ma[1] * ma[5] - ma[4] * ma[2] / det;
			mr[3] = -(ma[3] * ma[8] - ma[5] * ma[6]) / det;
			mr[4] = ma[0] * ma[8] - ma[6] * ma[2] / det;
			mr[5] = -(ma[0] * ma[5] - ma[3] * ma[2]) / det;
			mr[6] = ma[3] * ma[7] - ma[6] * ma[4] / det;
			mr[7] = -(ma[0] * ma[7] - ma[6] * ma[1]) / det;
			mr[8] = ma[0] * ma[4] - ma[1] * ma[3] / det;
			return;
		}
		public static float m3_det(ref Matrix mat)
		{
			float det;
			det = mat[0] * (mat[4] * mat[8] - mat[7] * mat[5])
			- mat[1] * (mat[3] * mat[8] - mat[6] * mat[5])
			+ mat[2] * (mat[3] * mat[7] - mat[6] * mat[4]);
			return (det);
		}
		//http://jeux.developpez.com/faq/math/?page=determinants_inverses#Q22
		public static void m4_submat(ref Matrix mr, ref Matrix mb, int i, int j)
		{
			int ti, tj, idst, jdst;
			jdst = 0;
			idst = 0;
			for (ti = 0; ti < 4; ti++)
			{
				if (ti < i)
					idst = ti;
				else if (ti > i)
					idst = ti - 1;
				for (tj = 0; tj < 4; tj++)
				{
					if (tj < j)
						jdst = tj;
					else
						if (tj > j)
							jdst = tj - 1;
					if (ti != i && tj != j)
						mb[idst * 3 + jdst] = mr[ti * 4 + tj];
				}
			}
		}
		public static float m4_det(ref Matrix mr)
		{
			float det, result = 0, i = 1;
			Matrix msub3 = new Matrix(3, 3);
			int n;
			for (n = 0; n < 4; n++, i *= -1)
			{
				m4_submat(ref mr, ref msub3, 0, n);
				det = m3_det(ref msub3);
				result += (mr)[n] * det * i;
			}
			return result;
		}

		public static int m4_inverse(ref Matrix mr, ref Matrix ma) // mr a l air de contenir le resultat;
		{
			float mdet = m4_det(ref ma);
			Matrix mtemp = new Matrix(4, 4);
			int i, j, sign;
			if (Math.Abs(mdet) < 0.0005)
			{
				return (0);
			}
			for (i = 0; i < 4; i++)
			{
				for (j = 0; j < 4; j++)
				{
					sign = 1 - ((i + j) % 2) * 2;
					m4_submat(ref ma, ref mtemp, i, j);
					mr[i + j * 4] = (m3_det(ref mtemp) * sign) / mdet;
				}
			}
			return (1);
		}
		public void Transpose()
		{
			Matrix res = new Matrix(this.height, this.width);
			for (int i = 0; i < this.width; i++)
			{
				for (int j = 0; j < this.height; j++)
				{
					res[i, j] = this[j, i];
				}
			}
			this.width = res.width;
			this.height = res.height;
			this._array = res._array;
		}
		public Matrix4 gl()
		{
			Matrix tmp = new Matrix(this);
			Matrix4 res = new Matrix4(tmp[0, 1 - 1], tmp[0, 2 - 1], tmp[0, 3 - 1], tmp[0, 4 - 1], tmp[1, 1 - 1], tmp[1, 2 - 1], tmp[1, 3 - 1], tmp[1, 4 - 1],
            tmp[2, 1 - 1], tmp[2, 2 - 1], tmp[2, 3 - 1], tmp[2, 4 - 1], tmp[3, 1 - 1], tmp[3, 2 - 1], tmp[3, 3 - 1], tmp[3, 4 - 1]);
			return (res);
		}

		public static Matrix lookAt(Vector3  Eye, Vector3  Center, Vector3  Up)
		{
			 Matrix res = Matrix.Identity;

			 Vector3 X, Y, Z;

			Z = Eye - Center;
			Z = Z.normalized;
			Y = Up;
			X = Y.Cross( Z );
			Y = Z.Cross( X );

			X = X.normalized;
			Y = Y.normalized;

			res[0, 0] = X.x;
			res[1, 0] = X.y;
			res[2, 0] = X.z;
			res[3, 0] = -X.Dot( Eye );
			res[0, 1] = Y.x;
			res[1, 1] = Y.y;
			res[2, 1] = Y.z;
			res[3, 1] = -Y.Dot( Eye );
			res[0, 2] = Z.x;
			res[1, 2] = Z.y;
			res[2, 2] = Z.z;
			res[3, 2] = -Z.Dot( Eye );
			res[0, 3] = 0;
			res[1, 3] = 0;
			res[2, 3] = 0;
			res[3, 3] = 1.0f;

			return (res);
		}
		public Matrix(Assimp.Matrix4x4 matrix)
		{
			this._array = Matrix.Identity.array;
			this.width = 4;
			this.height = 4;

			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					this[i, j] = matrix[i, j];
				}
			}
		}

		
		public static bool operator==(Matrix self, Matrix other)
		{
			if (self.width != other.width || other.height != self.height)
			{
				return (false);
			}
			for (int i = 0; i < self.width; i++ )
			{
				for (int j = 0; j < self.height; j++)
				{
					if (self[j, i] != other[j, i])
					{
						return (false);
					}
				}
			}
			return (true);
		}
		public static bool operator !=(Matrix self, Matrix other)
		{
			return (!(self == other));
		}
		public static Matrix Inverse2(Matrix matrix)
		{
			Matrix res = new Matrix(4, 4);
			Matrix tmp = M4x4(matrix);
			m4_inverse(ref res, ref tmp);
			
			return (res);
		}
		public static Matrix MatrixInverse(Matrix matrix)
		{
			return (Inverse2(matrix));
		}

		public Matrix(Matrix copy)
		{
			this._array = (float[])copy.array.Clone();
			this.width = copy.width;
			this.height = copy.height;
		}
		public Matrix inverseMatrix()
		{
			return (MatrixInverse(this));
		}

		public Matrix inverse
		{
			get
			{
				return (this.inverseMatrix());
			}
			set
			{
				;
			}
		}



		//Vector3			multiply(Vector3 point);
		//Quaternion		multiply(Quaternion quaternion);
		public Matrix multiply(Matrix matrix)
		{
			Matrix tmp = new Matrix(matrix);
			tmp = multiply(tmp, this);
			return (tmp);
		}

		//static Matrix	rotation(Quaternion quaternion);
		//static Matrix	rotation(Vector3 eulerAngles);
		public static Matrix rotation(float angle, int index)
		{
			if (index == 0)
			{
				return xRotation(angle);
			}
			if (index == 1)
			{
				return yRotation(angle);
			}
			if (index == 2)
			{
				return zRotation(angle);
			}
			return (xRotation(angle));
		}
		public static Matrix XYZEuler(float x, float y, float z)
		{
			return YPR(x, y, z);
		}
		public static Matrix YXZEuler(float x, float y, float z)
		{
			return YPR(y, x, z);
		}
		public static Matrix ZXYEuler(float x, float y, float z)
		{
			return YPR(z, x, y);
		}
		public static Matrix ZYXEuler(float x, float y, float z)
		{
			return YPR(z, y, x);
		}
		public static Matrix YZXEuler(float x, float y, float z)
		{
			return YPR(y, z, x);
		}
		public static Matrix XZYEuler(float x, float y, float z)
		{
			return YPR(x, z, y);
		}

		public static Matrix rotAround(float angle, Vector3 axis)
		{
		   float sinAngle, cosAngle;
		   axis = axis.normalized;
		   float x = axis.z;
		   float y = axis.y;
		   float z = axis.x;
		   float mag = axis.magnitude;//sqrtf(x * x + y * y + z * z);
 
		   sinAngle = (float)Math.Sin(angle * (Math.PI / 180.0f) );
		   cosAngle = (float)Math.Cos( angle * (Math.PI / 180.0f) );
		   if ( mag > Mathf.epsilon )
		   {
			  float xx, yy, zz, xy, yz, zx, xs, ys, zs;
			  float oneMinusCos;
			  Matrix res = Matrix.Identity;
 
			  x /= mag;
			  y /= mag;
			  z /= mag;
 
			  xx = x * x;
			  yy = y * y;
			  zz = z * z;
			  xy = x * y;
			  yz = y * z;
			  zx = z * x;
			  xs = x * sinAngle;
			  ys = y * sinAngle;
			  zs = z * sinAngle;
			  oneMinusCos = 1.0f - cosAngle;
 
			  res[0, 0] = (oneMinusCos * xx) + cosAngle;
			  res[0, 1] = (oneMinusCos * xy) - zs;
			  res[0, 2] = (oneMinusCos * zx) + ys;
			  res[0, 3] = 0.0F; 
 
			  res[1, 0] = (oneMinusCos * xy) + zs;
			  res[1, 1] = (oneMinusCos * yy) + cosAngle;
			  res[1, 2] = (oneMinusCos * yz) - xs;
			  res[1, 3] = 0.0F;
 
			  res[2, 0] = (oneMinusCos * zx) - ys;
			  res[2, 1] = (oneMinusCos * yz) + xs;
			  res[2, 2] = (oneMinusCos * zz) + cosAngle;
			  res[2, 3] = 0.0F; 
 
			  res[3, 0] = 0.0F;
			  res[3, 1] = 0.0F;
			  res[3, 2] = 0.0F;
			  res[3, 3] = 1.0F;
 
				return (res);
		   }
		   return (Identity);
		 }
		public static Matrix axisAngle(Vector3 axis, float angl)
		{
			float c = (float)Math.Cos(angl);
			float s = (float)Math.Sin(angl);
			float t = 1 - c;
			Vector3 normalizedAxis = axis.normalized;
			float x = normalizedAxis.x;
			float y = normalizedAxis.y;
			float z = normalizedAxis.z;
			//MessageBox.Show("axis :" + normalizedAxis);
			//MessageBox.Show("angl :" + angl);
			Matrix res = Matrix.Identity;
			res[0, 0] = t*x*x + c;	
			res[0, 1] = t*x*y - z*s;	
			res[0, 2] = t*x*z + y*s;
			
			res[1, 0] =	t*x*y + z*s; 
			res[1, 1] = t*y*y + c;	 
			res[1, 2] = t*y*z - x*s;
			
			res[2, 0] = t*x*z - y*s;	
			res[2, 1] = t*y*z + x*s; 	
			res[2, 2] = t*z*z + c;
			return (res);
		}
		public static Matrix xRotation(float xAngl)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{	1, 0, 0, 0,
							0, (float)Math.Cos(to_rad(xAngl)), -(float)Math.Sin(to_rad(xAngl)), 0,
							0, (float)Math.Sin(to_rad(xAngl)), (float)Math.Cos(to_rad(xAngl)), 0,
							0, 0, 0, 0 };
			return (res);
		}
		public static Matrix yRotation(float yAngl)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{	(float)Math.Cos(to_rad(yAngl)), 0, (float)Math.Sin(to_rad(yAngl)), 0,
							0, 1, 0, 0,
							-(float)Math.Sin(to_rad(yAngl)), 0, (float)Math.Cos(to_rad(yAngl)), 0,
							0, 0, 0, 1	};
			return (res);
		}
		public static Matrix zRotation(float zAngl)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{	(float)Math.Cos(to_rad(zAngl)), -(float)Math.Sin(to_rad(zAngl)), 0, 0,
							(float)Math.Sin(to_rad(zAngl)), (float)Math.Cos(to_rad(zAngl)), 0, 0,
							0, 0, 0, 0,
							0, 0, 0, 0	};
			return (res);
		}
		public static Matrix Projection(float viewAngle, float viewport, float near, float far)
		{
			Matrix res = new Matrix(4, 4);
			res.array = new float[]{	to_rad(viewAngle)/viewport, 0.0f, 0.0f, 0.0f,
							0.0f, to_rad(viewAngle), 0.0f, 0.0f,
							0.0f, 0.0f, (far + near) /(near - far), (2.0f * (far) * near) / (near - far),
							0.0f, 0.0f, -1.0f, 0.0f};
			return (res);
		}
		public Matrix transposed()
		{
			Matrix res = new Matrix(this.width, this.height);
			for (int i = 0; i < this.height; i++)
			{
				for (int j = 0; j < this.width; j++)
				{
					res[j, i] = (this)[i, j];
				}
			}
			return (res);
		}

	}
}
