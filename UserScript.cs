﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Policy;
using System.Security;
using System.Security.Permissions;
//using Mono.Cecil;
using FastMember;
using CSScriptLibrary;
//using Microsoft.CodeAnalysis;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
//using Microsoft.CodeAnalysis.Chsarp;
//using Microsoft.CSharp;
//using Roslyn;
namespace OctogonEngine
{
	[AttributeUsage(AttributeTargets.Assembly)]
	public class FileName : Attribute
	{
		string someText;
		public FileName() : this(string.Empty) { }
		public FileName(string txt) { someText = txt; }
		//...
	}

	public class CustomProperty
	{
		//public bool expanded = false;
        //Session session = new Session();
        //Microsoft.CSharp.CSharpCodeProvider;
		public CustomProperty root
		{
			get
			{
				CustomProperty tmp = this;
				while (tmp.parentObject != null)
				{
					tmp = tmp.parentObject;
				}
				return (tmp);
			}
		}
		private bool _updated = false;
		public bool updated
		{
			get
			{
				//Console.WriteLine("HEY\n");
				if (this._parentObject == null)
				{
					Console.WriteLine("HEY3\n");
					return (this._updated);
				}
				else
				{
					Console.WriteLine("HEY4\n");
					return (this._parentObject.updated);
				}
			}
			set
			{
				if (this._parentObject == null)
				{
					this._updated = value;
				}
				else
				{
					this._parentObject.updated = value;
				}
			}
		}
		private object _value = null;
		private CustomProperty _parentObject = null;
		private string _name = "";
		public bool expanded;
		public CustomProperty parentObject
		{
			get
			{
				return (this._parentObject);
			}
			private set
			{
				this._parentObject = value;
			}
		}
		public string name
		{
			get
			{
				return (this._name);
			}
			private set
			{
				this._name = value;
			}
		}
		public object value
		{
			get
			{
				return (this._value);
			}
			set
			{
				if (this.parentObject != null)
					this.SetPropValue((object)value);
			}
		}
		private List<CustomProperty> _sons = null;
		public List<CustomProperty> GetSons()
		{
			if (this._sons == null)
			{
				this._sons = new List<CustomProperty>();
				List<ObjectGroup> properties = UserScript.Serialize(this.value);
				for (int i = 0; i < properties.Count; i++)
				{
					this._sons.Add(new CustomProperty(this, properties[i].name, properties[i].value));
				}
				//this._sons = res;
				return (this._sons);
			}
			else
			{
				//Debug.Log("OHHHHHEEEYYYGETSONS");
				return (this._sons);
			}
		}
		public void SetValue(object value)
		{
			this.value = value;
		}
		public void SetPropValue(object value)
		{
			List<CustomProperty> hierarchy = new List<CustomProperty>();
			CustomProperty tmp = this;
			if (tmp.parentObject != null)
			{
				tmp.parentObject.SetValue(this.parentObject.value.SetPropValue(this.name, this.value));
			}
			else
			{
				return;//this.value = value;
			}
			//this.value = value;
			if (!tmp.parentObject.value.IsValueType())
			{
				return;
			}
			else
			{
				//hierarchy.Add(this);
				while (tmp.parentObject != null && tmp.parentObject.IsValueType())
				{
					

					//hierarchy.Add(tmp..parentObject);
					tmp = tmp.parentObject;
					tmp.parentObject.SetValue(tmp.parentObject.value.SetPropValue(tmp.name, tmp.value));

				}
				if (tmp.parentObject != null)
				{
					tmp.parentObject.SetValue(tmp.parentObject.value.SetPropValue(tmp.name, tmp.value));
				}

			}
			/*string hierarchyString = "";
			hierarchy.Reverse();
			object grandDaddy = tmp.value;
			for (int i = 1; i < hierarchy.Count; i++)
			{
				if (i < hierarchy.Count - 1)
				{
					hierarchyString += hierarchy[i].name + ".";
				}
				else
				{
					hierarchyString += hierarchy[i].name;
				}
			}
		
			string scriptCode = "using System;\n using OctogonEngine;using System.Reflection;  using System.Runtime;                     "+
				"public class ValueSetter                      " +
							"{                                      " +
							"   public void SetValue(object value, object greatDaddy) " +
							"   {             dynamic grandDaddy = greatDaddy;  dynamic value2 = value;                    " +
							"		(grandDaddy)." + hierarchyString + "= value2;" +
							"	}" + 
							"}";

			Dictionary<string, string> providerOptions = new Dictionary<string, string>
                {
                    {"CompilerVersion", "v4.0"}
                };
			CSharpCodeProvider provider = new CSharpCodeProvider(providerOptions);

			CompilerParameters compilerParams = new CompilerParameters
			{
				GenerateInMemory = true,
				GenerateExecutable = false,
				IncludeDebugInformation = true
			};
			compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "OctogonEngine.dll"));
			compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AssimpNet.dll"));
			compilerParams.ReferencedAssemblies.Add("System.dll");
			compilerParams.ReferencedAssemblies.Add("System.Core.dll");
			compilerParams.ReferencedAssemblies.Add("System.Reflection.Metadata.dll");
			compilerParams.ReferencedAssemblies.Add("Microsoft.CSharp.dll");
			//compilerParams.ReferencedAssemblies.Add("System.Runtime.dll");
			//compilerParams.ReferencedAssemblies.Add("System.Reflection.dll");
			//compilerParams.ReferencedAssemblies.Add("AssimpNet.dll");
			String text = scriptCode;
			CompilerResults results = provider.CompileAssemblyFromSource(compilerParams, text);
			if (results.Errors.Count != 0)
			{
				foreach (var error in results.Errors)
				{

					Console.WriteLine(error.ToString());
				}

				Console.WriteLine("failed");
			}
			else
			{
				//Debug.Log("oheyyy");
				//                throw new Exception("Mission failed!");
				//className = Path.GetFileNameWithoutExtension(path);
				Type type = results.CompiledAssembly.GetType("ValueSetter");
				dynamic valueSetter = Activator.CreateInstance(type);
				valueSetter.SetValue(this.value, grandDaddy);
			}
			//UserScript.scriptDictionary[results.CompiledAssembly] = this;
			//Debug.Log("TYPE:" + type.Name);*/
			
		}
		public CustomProperty(CustomProperty parentObject, string name, object value)
		{
			this._parentObject = parentObject;
			this._name = name;
			this._value = value;
		}
	}
	public class Proxy : MarshalByRefObject
	{
		public AppDomain check;
		public Assembly GetAssembly(string assemblyPath)
		{
			try
			{
				return Assembly.ReflectionOnlyLoadFrom(assemblyPath);
			}
			catch (Exception)
			{
				return null;
				// throw new InvalidOperationException(ex);
			}
		}
	}
	public class UserScript
	{

		public AppDomain	domain;
		public Assembly		assembly;
		public Type			type;
		//private String		_dllPath;
		public String		dllPath;
		public String		scriptPath;
		public String		className;
		public String		file;

		public static Dictionary<Assembly, UserScript> scriptDictionary = new Dictionary<Assembly, UserScript>();
		public static List<UserScript> scripts = new List<UserScript>();

		public static bool Contains(String className)
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				if (UserScript.scripts[i].className == className)
				{
					return (true);
				}
			}
			return (false);
		}

		public static UserScript Find(String className)
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				if (UserScript.scripts[i].className == className)
				{
					return (UserScript.scripts[i]);
				}
			}
			return (null);
		}
		private static object _tmp;
		public static object tmp
		{
			get
			{
				return (_tmp);
			}
			private set
			{
				_tmp = value;
			}
		}
		public static ObjectGroup GetProperty(object obj, string name)
		{
            Debug.Log("CHECKING:" + name);
				tmp = obj;
				dynamic callingObj = obj;
				//dynamic accessor = TypeAccessor.Create(((dynamic)callingObj).GetType());
				string propName = name;
				return (null);
//				return (new ObjectGroup(name, accessor[(dynamic)callingObj, propName]));
		}

		public static bool SetProperty(object callingObj, string name, object value)
		{
				Debug.Log("callingObject:" + callingObj.GetType().Name);
				Debug.Log("ok:" + value + ";" + "name:" + name);
				tmp = callingObj;
				TypeAccessor accessor = TypeAccessor.Create(((dynamic)callingObj).GetType());
            string propName = name;
          



				accessor[(dynamic)callingObj, propName] = value;
				/*string scriptCode = "using System;\n using OctogonEngine;                        " +
							"public class ValueSetter                      " +
							"{                                      " +
							"   public void SetValue(object value) " +
							"   {                                   " +
							 "((dynamic)OctogonEngine.UserScript.tmp)" + "." + name + " = value" + ";" +
							"   }                                   " +
							"}";
				Debug.Log(GetProperty(callingObj, name).value.ToString());
				//AsmHelper helper = new AsmHelper(CSScript.LoadCode(scriptCode, null, false));
				//helper.Invoke("ValueSetter.SetValue", value);
				dynamic instance = CSScript.Evaluator.LoadCode(scriptCode);//.CreateInstance("ValueSetter");
				instance.SetValue(value);*/
				return (true);
		}



		public static List<ObjectGroup> GetProperties(object obj)
		{
			return (Serialize(obj));
		}
		public static List<ObjectGroup> Serialize(object obj)
		{
			Console.WriteLine("what?");
			if (obj != null)
			{
                List<ObjectGroup> res = new List<ObjectGroup>();
                //TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType());
            
               /* var values = accessor.GetMembers();//.Select(member => accessor[ynamic)obj, member.Name]).ToList();
                for (int i = 0; i < values.Count; i++)
                {
                    try
                    {
                        object value = accessor[obj, values[i].Name];
                        //Debug.Log("ADDED:" + values[i].Name + ";");
                        res.Add(new ObjectGroup(values[i].Name, value));
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                }*/
                /*
                                Console.WriteLine("ohhhh");
                                UserScript test = UserScript.Find(obj.GetType().Name);
                                    Console.WriteLine("heyyyy");
                                    PropertyInfo[] infostmp = obj.GetType().GetProperties();
                                    List<ObjectGroup> res = new List<ObjectGroup>();
                                    if (infostmp != null)
                                    {
                                        List<PropertyInfo> infos = infostmp.ToList();
                                        for (int i = 0; i < infos.Count; i++)
                                        {
                                            Console.WriteLine("dafuck?");
                                            try
                                            {
                                                res.Add(new ObjectGroup(infos[i].Name, GetPropValue(obj, infos[i].Name)));
                                            }
                                            catch (Exception e)
                                            {
                                                continue;
                                            }
                                        }
                                    Console.WriteLine("end...");
                                    return (res);
                    */
                return (res);
			}
			return (null);
		}
		public static object GetPropValue(object src, string propName, bool dynamic)
		{
			if (!dynamic)
			{

				return src.GetType().GetProperty(propName).GetValue(src, null);
			}
			else
			{
				UserScript test = null;
				if (UserScript.scriptDictionary.TryGetValue(src.GetType().Assembly, out test))
				{
					return GetProperty(src, propName).value;
				}
				else
				{
					return src.GetType().GetProperty(propName).GetValue(src, null);
				}
			}
		}
		public static bool SetPropValue(object src, string propName, object value)
		{
			return (SetProperty(src, propName, value));
		}
		public static bool SetPropValue(object src, string propName, object value, bool dynamic)
		{
			return (SetProperty(src, propName, value));
		}
		public static object GetPropValue(object src, string propName)
		{
			if (src.GetType().Assembly == Assembly.GetExecutingAssembly())
				return src.GetType().GetProperty(propName).GetValue(src, null);
			else
			{
				UserScript test = UserScript.Find(src.GetType().Name);
				if (test != null)
				{
					return GetProperty(src, propName).value;
				}
				else
				{
					return src.GetType().GetProperty(propName).GetValue(src, null);
				}
			}
		}
		public bool Exist()
		{
			try
				{
					System.Console.WriteLine("Child domain: " + domain.FriendlyName);
				}
				catch (System.AppDomainUnloadedException e)
				{
					return (false);
				}
			if (!File.Exists(this.scriptPath))
			{
				this.Unload();
				return (false);
			}
			return (true);
		}

		public List<Transform> Unload()
		{
			List<Transform> affecteds = new List<Transform>();
			if (this.type != null)
			{
				for (int i = 0; i < Transform.Transforms.Count; i++)
				{
					Transform mb = Transform.Transforms[i];

					if (mb != null && mb.hasScript(this.className))
					{
						((MonoBehaviour)mb).removeComponents(this.type);
						affecteds.Add(mb);
					}
				}
			}
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			this.type = null;
			scripts.Remove(this);
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			return (affecteds);

		}
		public uint nameExtension = 0;
		public static void CheckUpdates()
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				UserScript.scripts[i].checkUpdate();
			}
		}
		public void checkUpdate()
		{
			if (File.Exists(this.scriptPath))
			{
				try
				{
					//if (checkPermissions(this.scriptPath))
					//{
					String tmp = File.ReadAllText(this.scriptPath);
					if (tmp != file)
					{
						this.ReLoad();
					}
					//}
				}
				catch (IOException ioEx)
				{
					;
				}
			}
		}
		public void ReLoad()
		{

			List<Transform> affecteds = this.Unload();
			scripts.Add(this);
			this.Load();
			for (int i = 0; i < affecteds.Count; i++)
			{
				//Debug.Log("oheyyy");
				((MonoBehaviour)affecteds[i]).AddComponent(this.type);
			}
		}
		private bool Compile(String path)
		{
            Dictionary<string, string> providerOptions = new Dictionary<string, string>
                {
                    {"CompilerVersion", "v3.5"}
                };
            CSharpCodeProvider provider = new CSharpCodeProvider(providerOptions);

            CompilerParameters compilerParams = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false,
				IncludeDebugInformation = true
            };
            compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "OctogonEngine.dll"));
            compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AssimpNet.dll"));
            compilerParams.ReferencedAssemblies.Add("System.dll");
            compilerParams.ReferencedAssemblies.Add("System.Core.dll");
            //compilerParams.ReferencedAssemblies.Add("AssimpNet.dll");
            String text = File.ReadAllText(path);
            CompilerResults results = provider.CompileAssemblyFromSource(compilerParams, text);
            if (results.Errors.Count != 0)
            {
                foreach (var error in results.Errors)
                {
                    Debug.Log(error.ToString());
                }

                Debug.Log("failed");
            }
            Debug.Log("oheyyy");
//                throw new Exception("Mission failed!");
            className = Path.GetFileNameWithoutExtension(path);
            type = results.CompiledAssembly.GetType(className);
			UserScript.scriptDictionary[results.CompiledAssembly] = this;
            Debug.Log("TYPE:" + type.Name);
         //  type = o.GetType();
            //mi.Invoke(o, null);

            /*
            String dir;
			ProcessStartInfo start = new ProcessStartInfo();
			dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var permissionSet = new PermissionSet(PermissionState.None);
			dir = Path.Combine(dir, Path.GetFileNameWithoutExtension(path) + ".dll");
			if (File.Exists(dir))
			{
				File.Delete(dir);
			}
			this.dllPath = dir;
			dir = this.dllPath;
			start.FileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Mono\\lib\\mono\\4.5\\mcs.exe");
			start.UseShellExecute = false;
			start.RedirectStandardError = true;
			start.RedirectStandardOutput = true;
			start.Arguments = "\"" + path + "\" " + "/target:library" + " " + "/out:" + "\"" + dir + "\"" + " " + "/reference:OctogonEngine.dll" + " /reference:AssimpNet.dll";
			using (Process process = Process.Start(start))
			{
				using (StreamReader reader = process.StandardError)
				{
					string result = reader.ReadToEnd();
					if (result.Length > 0)
						Debug.Log(result);
				}
				using (StreamReader reader = process.StandardOutput)
				{
					string result = reader.ReadToEnd();
					if (result.Length > 0)
						Debug.Log(result);
				}
			}
			//Debug.Log("compilation ok");*/
			return (true);
		}

		private void Compile2(String scriptPath)
		{

		}

		public void Load()
		{
            //Debug.Log("Load");
            Debug.Log("HEYYYYWTF");
			if (Compile(scriptPath))
			{
					String dir = Path.GetDirectoryName(this.dllPath);
						//File.Delete(dir);
					//dir = Path.Combine(dir, Path.GetFileNameWithoutExtension(this.dllPath) + ".dll");
					//className = Path.GetFileNameWithoutExtension(this.dllPath);
					/*byte[] raw = File.ReadAllBytes(this.dllPath);
					Assembly assembly = AppDomain.CurrentDomain.Load(raw);
					type = assembly.GetType(className);
				*/	Debug.Log("TYPENAME:" + type.Name);
					scripts.Add(this);
					if (File.Exists(scriptPath))
						this.file = File.ReadAllText(scriptPath);
			}
		}
		public static List<String> Trash = new List<String>();

		public UserScript(String scriptPath)
		{

			if (File.Exists(scriptPath))
				this.file = File.ReadAllText(scriptPath);

			//this.file
			//Debug.Log("yoyo");
			this.scriptPath = scriptPath;
			this.Load();
			
		}

	}
}
