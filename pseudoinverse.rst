function Y = geninv(G)
// Returns the Moore-Penrose inverse of the argument
// Transpose if m < n
Matrix getPseudoInverse(Matrix G)
{
m = G.height;
n = G.width;
bool transpose=false;
if (m < n)
{
 	transpose=true;
 	Matrix A = G * G.transpose;
 	n = m;
}
else
{
	Matrix A = G.transpose * G;
}

float[] dA = Matrix.getMainDiagonal(A);
// (tol = le m)
float tol = Mathf.getNonZeroMinimum(dA) * 1e-9;
L = Matrix.Zero;;
r = 0;
for (int k = 1; k < n; k++)
{
	r = r + 1;
	for (int j = k; j < n; j++)
	{
		float tmp;
		for (int l = 1; l < r - 1; l++)
		{
			tmp += L[j, l];
		}
		float tmp2;
		for (int o = 1; o  < r - 1; o++)
		{
			tmp2 += L.transpose[k, o];
		}
		L[j, r] = A[j, k] - (tmp * tmp2);
	}
	//% Note: for r=1, the substracted vector is zero
	if (L[k, r] > tol)
	{
		L[k, r]=Mathf.Sqrt(L[k, r]);
		if (k <n)
		{
			for (int j = k + 1; j < n; j++)
			{
				L[j,r] = L[j, r] / L[k, r];
			}
		}
	}
	else
	{
 		r = r - 1;
 	}
}
Matrix res = new Matrix(L.height, r - 1);
for (int i = 0; i < L.height; i++)
{
	for (int j = 1; j < r; j++)
	{
		res[i, j - 1] = L[i, j]; 
	}
}
Matrix M = (res.transpose * res).inverse;
 //% Computation of the generalized inverse of G
//res = 
if (transpose)
{
	res = G.transpose * res * M * M * L.transpose;
}
else
{
	res = L * M * M * L.transpose * G.transpose;
}
}