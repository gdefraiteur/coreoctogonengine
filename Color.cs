/*
Ce projet appartient au groupe Octogon

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

beware ur ass if u do stupid things.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

namespace OctogonEngine
{
public struct Color
{
	public Color(float r, float g, float b, float a)
	{
		this[0] = r;
		this[1] = g;
		this[2] = b;
		this[3] = a;
	}

	public static Color intColor(int r, int g, int b, int a)
	{
		this[0] = ((float)r) / 255;
		this[1] = ((float)g) / 255;
		this[2] = ((float)b) / 255;
		this[3] = ((float)a) / 255;
	}

	public float this[int index]
	{
		get
		{
			return (this._array[index]);
		}
		set
		{
			this._array[index] = value;
		}
	}

	private float[] _array = new float[4];
	public float[] array
	{
		get
		{
			return (this._array.Clone());
		}
		set
		{
			this._array = value;
		}
	}

	public float R
	{
		get
		{
			return (this[0]);
		}
		set
		{
			this[0] = value;
		}
	}

	public float G
	{
		get
		{
			return (this[1]);
		}
		set
		{
			this[1] = value;
		}
	}

	public float B
	{
		get
		{
			return (this[2]);
		}
		set
		{
			this[2] = value;
		}
	}

	public float A
	{
		get
		{
			return (this[3]);
		}
		set
		{
			this[3] = value;
		}
	}

	public float r
	{
		get
		{
			return (this[0]);
		}
		set
		{
			this[0] = value;
		}
	}

	public float g
	{
		get
		{
			return (this[1]);
		}
		set
		{
			this[1] = value;
		}
	}

	public float b
	{
		get
		{
			return (this[2]);
		}
		set
		{
			this[2] = value;
		}
	}

	public float a
	{
		get
		{
			return (this[3]);
		}
		set
		{
			this[3] = value;
		}
	}

	public static Color Normalize(Color vector)
	{
		return (vector / vector.Length());
	}
	public static Color operator-(Color self, Color other)
	{
		return (new Color(self.r - other.r, self.g - other.g, self.b - other.b, self.a - other.a));
	}
	public static Color operator*(Color self, float other)
	{
		return (new Color(self.r * other, self.g * other, self.b * other, self.a * other));
	}
	public static Color operator /(Color self, float other)
	{
		return (new Color(self.r / other, self.g / other, self.b / other, self.a / other));
	}
	public static Color operator *(float other, Color self)
	{
		return (new Color(self.r * other, self.g * other, self.b * other, self.a * other));
	}
	public static Color operator *(Color self, Color other)
	{
		return (new Color(self.r * other.r, self.g * other.g, self.b * other.b, self.a * other.a));
	}
	public static Color operator /(Color self, Color other)
	{
		Color rectified = other;
		if (other.r == 0)
		{
			rectified.r = 0.0001f;
		}
		if (other.g == 0)
		{
			rectified.g = 0.0001f;
		}
		if (other.b == 0)
		{
			rectified.b = 0.0001f;
		}
		if (other.a == 0)
		{
			rectified.a = 0.0001f;
		}
		return (new Color(self.r / other.r, self.g / other.g, self.b / other.b));
	}
	public static Color operator+(Color self, Color other)
	{
		return (new Color(self.r + other.r, self.g + other.g, self.b + other.b, self.a + other.a));
	}

	public static Color4 gl()
	{
		Color4 c;
		c.R = color.R;
		c.G = color.G;
		c.B = color.B;
		c.A = color.A;
		return c;
	}

	public static Color AliceBlue
	{
		get
		{
	 		return (Color.intColor(240,248,255,255));
		}
	}
	public static Color LightSalmon
	{
		get
		{
	 		return (Color.intColor(255,160,122,255));
		}
	}
	public static Color AntiqueWhite
	{
		get
		{
	 		return (Color.intColor(250,235,215,255));
		}
	}
	public static Color LightSeaGreen
	{
		get
		{
	 		return (Color.intColor(32,178,170,255));
		}
	}
	public static Color Aqua
	{
		get
		{
	 		return (Color.intColor(0,255,255,255));
		}
	}
	public static Color LightSkyBlue
	{
		get
		{
	 		return (Color.intColor(135,206,250,255));
		}
	}
	public static Color Aquamarine
	{
		get
		{
	 		return (Color.intColor(127,255,212,255));
		}
	}
	public static Color LightSlateGray
	{
		get
		{
	 		return (Color.intColor(119,136,153,255));
		}
	}
	public static Color Azure
	{
		get
		{
	 		return (Color.intColor(240,255,255,255));
		}
	}
	public static Color LightSteelBlue
	{
		get
		{
	 		return (Color.intColor(176,196,222,255));
		}
	}
	public static Color Beige
	{
		get
		{
	 		return (Color.intColor(245,245,220,255));
		}
	}
	public static Color LightYellow
	{
		get
		{
	 		return (Color.intColor(255,255,224,255));
		}
	}
	public static Color Bisque
	{
		get
		{
	 		return (Color.intColor(255,228,196,255));
		}
	}
	public static Color Lime
	{
		get
		{
	 		return (Color.intColor(0,255,0,255));
		}
	}
	public static Color Black
	{
		get
		{
	 		return (Color.intColor(0,0,0,255));
		}
	}
	public static Color LimeGreen
	{
		get
		{
	 		return (Color.intColor(50,205,50,255));
		}
	}
	public static Color BlanchedAlmond
	{
		get
		{
	 		return (Color.intColor(255,255,205,255));
		}
	}
	public static Color Linen
	{
		get
		{
	 		return (Color.intColor(250,240,230,255));
		}
	}
	public static Color Blue
	{
		get
		{
	 		return (Color.intColor(0,0,255,255));
		}
	}
	public static Color Magenta
	{
		get
		{
	 		return (Color.intColor(255,0,255,255));
		}
	}
	public static Color BlueViolet
	{
		get
		{
	 		return (Color.intColor(138,43,226,255));
		}
	}
	public static Color Maroon
	{
		get
		{
	 		return (Color.intColor(128,0,0,255));
		}
	}
	public static Color Brown
	{
		get
		{
	 		return (Color.intColor(165,42,42,255));
		}
	}
	public static Color MediumAquamarine
	{
		get
		{
	 		return (Color.intColor(102,205,170,255));
		}
	}
	public static Color BurlyWood
	{
		get
		{
	 		return (Color.intColor(222,184,135,255));
		}
	}
	public static Color MediumBlue
	{
		get
		{
	 		return (Color.intColor(0,0,205,255));
		}
	}
	public static Color CadetBlue
	{
		get
		{
	 		return (Color.intColor(95,158,160,255));
		}
	}
	public static Color MediumOrchid
	{
		get
		{
	 		return (Color.intColor(186,85,211,255));
		}
	}
	public static Color Chartreuse
	{
		get
		{
	 		return (Color.intColor(127,255,0,255));
		}
	}
	public static Color MediumPurple
	{
		get
		{
	 		return (Color.intColor(147,112,219,255));
		}
	}
	public static Color Chocolate
	{
		get
		{
	 		return (Color.intColor(210,105,30,255));
		}
	}
	public static Color MediumSeaGreen
	{
		get
		{
	 		return (Color.intColor(60,179,113,255));
		}
	}
	public static Color Coral
	{
		get
		{
	 		return (Color.intColor(255,127,80,255));
		}
	}
	public static Color MediumSlateBlue
	{
		get
		{
	 		return (Color.intColor(123,104,238,255));
		}
	}
	public static Color CornflowerBlue
	{
		get
		{
	 		return (Color.intColor(100,149,237,255));
		}
	}
	public static Color MediumSpringGreen
	{
		get
		{
	 		return (Color.intColor(0,250,154,255));
		}
	}
	public static Color Cornsilk
	{
		get
		{
	 		return (Color.intColor(255,248,220,255));
		}
	}
	public static Color MediumTurquoise
	{
		get
		{
	 		return (Color.intColor(72,209,204,255));
		}
	}
	public static Color Crimson
	{
		get
		{
	 		return (Color.intColor(220,20,60,255));
		}
	}
	public static Color MediumVioletRed
	{
		get
		{
	 		return (Color.intColor(199,21,112,255));
		}
	}
	public static Color Cyan
	{
		get
		{
	 		return (Color.intColor(0,255,255,255));
		}
	}
	public static Color MidnightBlue
	{
		get
		{
	 		return (Color.intColor(25,25,112,255));
		}
	}
	public static Color DarkBlue
	{
		get
		{
	 		return (Color.intColor(0,0,139,255));
		}
	}
	public static Color MintCream
	{
		get
		{
	 		return (Color.intColor(245,255,250,255));
		}
	}
	public static Color DarkCyan
	{
		get
		{
	 		return (Color.intColor(0,139,139,255));
		}
	}
	public static Color MistyRose
	{
		get
		{
	 		return (Color.intColor(255,228,225,255));
		}
	}
	public static Color DarkGoldenrod
	{
		get
		{
	 		return (Color.intColor(184,134,11,255));
		}
	}
	public static Color Moccasin
	{
		get
		{
	 		return (Color.intColor(255,228,181,255));
		}
	}
	public static Color DarkGray
	{
		get
		{
	 		return (Color.intColor(169,169,169,255));
		}
	}
	public static Color NavajoWhite
	{
		get
		{
	 		return (Color.intColor(255,222,173,255));
		}
	}
	public static Color DarkGreen
	{
		get
		{
	 		return (Color.intColor(0,100,0,255));
		}
	}
	public static Color Navy
	{
		get
		{
	 		return (Color.intColor(0,0,128,255));
		}
	}
	public static Color DarkKhaki
	{
		get
		{
	 		return (Color.intColor(189,183,107,255));
		}
	}
	public static Color OldLace
	{
		get
		{
	 		return (Color.intColor(253,245,230,255));
		}
	}
	public static Color DarkMagena
	{
		get
		{
	 		return (Color.intColor(139,0,139,255));
		}
	}
	public static Color Olive
	{
		get
		{
	 		return (Color.intColor(128,128,0,255));
		}
	}
	public static Color DarkOliveGreen
	{
		get
		{
	 		return (Color.intColor(85,107,47,255));
		}
	}
	public static Color OliveDrab
	{
		get
		{
	 		return (Color.intColor(107,142,45,255));
		}
	}
	public static Color DarkOrange
	{
		get
		{
	 		return (Color.intColor(255,140,0,255));
		}
	}
	public static Color Orange
	{
		get
		{
	 		return (Color.intColor(255,165,0,255));
		}
	}
	public static Color DarkOrchid
	{
		get
		{
	 		return (Color.intColor(153,50,204,255));
		}
	}
	public static Color OrangeRed
	{
		get
		{
	 		return (Color.intColor(255,69,0,255));
		}
	}
	public static Color DarkRed
	{
		get
		{
	 		return (Color.intColor(139,0,0,255));
		}
	}
	public static Color Orchid
	{
		get
		{
	 		return (Color.intColor(218,112,214,255));
		}
	}
	public static Color DarkSalmon
	{
		get
		{
	 		return (Color.intColor(233,150,122,255));
		}
	}
	public static Color PaleGoldenrod
	{
		get
		{
	 		return (Color.intColor(238,232,170,255));
		}
	}
	public static Color DarkSeaGreen
	{
		get
		{
	 		return (Color.intColor(143,188,143,255));
		}
	}
	public static Color PaleGreen
	{
		get
		{
	 		return (Color.intColor(152,251,152,255));
		}
	}
	public static Color DarkSlateBlue
	{
		get
		{
	 		return (Color.intColor(72,61,139,255));
		}
	}
	public static Color PaleTurquoise
	{
		get
		{
	 		return (Color.intColor(175,238,238,255));
		}
	}
	public static Color DarkSlateGray
	{
		get
		{
	 		return (Color.intColor(40,79,79,255));
		}
	}
	public static Color PaleVioletRed
	{
		get
		{
	 		return (Color.intColor(219,112,147,255));
		}
	}
	public static Color DarkTurquoise
	{
		get
		{
	 		return (Color.intColor(0,206,209,255));
		}
	}
	public static Color PapayaWhip
	{
		get
		{
	 		return (Color.intColor(255,239,213,255));
		}
	}
	public static Color DarkViolet
	{
		get
		{
	 		return (Color.intColor(148,0,211,255));
		}
	}
	public static Color PeachPuff
	{
		get
		{
	 		return (Color.intColor(255,218,155,255));
		}
	}
	public static Color DeepPink
	{
		get
		{
	 		return (Color.intColor(255,20,147,255));
		}
	}
	public static Color Peru
	{
		get
		{
	 		return (Color.intColor(205,133,63,255));
		}
	}
	public static Color DeepSkyBlue
	{
		get
		{
	 		return (Color.intColor(0,191,255,255));
		}
	}
	public static Color Pink
	{
		get
		{
	 		return (Color.intColor(255,192,203,255));
		}
	}
	public static Color DimGray
	{
		get
		{
	 		return (Color.intColor(105,105,105,255));
		}
	}
	public static Color Plum
	{
		get
		{
	 		return (Color.intColor(221,160,221,255));
		}
	}
	public static Color DodgerBlue
	{
		get
		{
	 		return (Color.intColor(30,144,255,255));
		}
	}
	public static Color PowderBlue
	{
		get
		{
	 		return (Color.intColor(176,224,230,255));
		}
	}
	public static Color Firebrick
	{
		get
		{
	 		return (Color.intColor(178,34,34,255));
		}
	}
	public static Color Purple
	{
		get
		{
	 		return (Color.intColor(128,0,128,255));
		}
	}
	public static Color FloralWhite
	{
		get
		{
	 		return (Color.intColor(255,250,240,255));
		}
	}
	public static Color Red
	{
		get
		{
	 		return (Color.intColor(255,0,0,255));
		}
	}
	public static Color ForestGreen
	{
		get
		{
	 		return (Color.intColor(34,139,34,255));
		}
	}
	public static Color RosyBrown
	{
		get
		{
	 		return (Color.intColor(188,143,143,255));
		}
	}
	public static Color Fuschia
	{
		get
		{
	 		return (Color.intColor(255,0,255,255));
		}
	}
	public static Color RoyalBlue
	{
		get
		{
	 		return (Color.intColor(65,105,225,255));
		}
	}
	public static Color Gainsboro
	{
		get
		{
	 		return (Color.intColor(220,220,220,255));
		}
	}
	public static Color SaddleBrown
	{
		get
		{
	 		return (Color.intColor(139,69,19,255));
		}
	}
	public static Color GhostWhite
	{
		get
		{
	 		return (Color.intColor(248,248,255,255));
		}
	}
	public static Color Salmon
	{
		get
		{
	 		return (Color.intColor(250,128,114,255));
		}
	}
	public static Color Gold
	{
		get
		{
	 		return (Color.intColor(255,215,0,255));
		}
	}
	public static Color SandyBrown
	{
		get
		{
	 		return (Color.intColor(244,164,96,255));
		}
	}
	public static Color Goldenrod
	{
		get
		{
	 		return (Color.intColor(218,165,32,255));
		}
	}
	public static Color SeaGreen
	{
		get
		{
	 		return (Color.intColor(46,139,87,255));
		}
	}
	public static Color Gray
	{
		get
		{
	 		return (Color.intColor(128,128,128,255));
		}
	}
	public static Color Seashell
	{
		get
		{
	 		return (Color.intColor(255,245,238,255));
		}
	}
	public static Color Green
	{
		get
		{
	 		return (Color.intColor(0,128,0,255));
		}
	}
	public static Color Sienna
	{
		get
		{
	 		return (Color.intColor(160,82,45,255));
		}
	}
	public static Color GreenYellow
	{
		get
		{
	 		return (Color.intColor(173,255,47,255));
		}
	}
	public static Color Silver
	{
		get
		{
	 		return (Color.intColor(192,192,192,255));
		}
	}
	public static Color Honeydew
	{
		get
		{
	 		return (Color.intColor(240,255,240,255));
		}
	}
	public static Color SkyBlue
	{
		get
		{
	 		return (Color.intColor(135,206,235,255));
		}
	}
	public static Color HotPink
	{
		get
		{
	 		return (Color.intColor(255,105,180,255));
		}
	}
	public static Color SlateBlue
	{
		get
		{
	 		return (Color.intColor(106,90,205,255));
		}
	}
	public static Color IndianRed
	{
		get
		{
	 		return (Color.intColor(205,92,92,255));
		}
	}
	public static Color SlateGray
	{
		get
		{
	 		return (Color.intColor(112,128,144,255));
		}
	}
	public static Color Indigo
	{
		get
		{
	 		return (Color.intColor(75,0,130,255));
		}
	}
	public static Color Snow
	{
		get
		{
	 		return (Color.intColor(255,250,250,255));
		}
	}
	public static Color Ivory
	{
		get
		{
	 		return (Color.intColor(255,240,240,255));
		}
	}
	public static Color SpringGreen
	{
		get
		{
	 		return (Color.intColor(0,255,127,255));
		}
	}
	public static Color Khaki
	{
		get
		{
	 		return (Color.intColor(240,230,140,255));
		}
	}
	public static Color SteelBlue
	{
		get
		{
	 		return (Color.intColor(70,130,180,255));
		}
	}
	public static Color Lavender
	{
		get
		{
	 		return (Color.intColor(230,230,250,255));
		}
	}
	public static Color Tan
	{
		get
		{
	 		return (Color.intColor(210,180,140,255));
		}
	}
	public static Color LavenderBlush
	{
		get
		{
	 		return (Color.intColor(255,240,245,255));
		}
	}
	public static Color Teal
	{
		get
		{
	 		return (Color.intColor(0,128,128,255));
		}
	}
	public static Color LawnGreen
	{
		get
		{
	 		return (Color.intColor(124,252,0,255));
		}
	}
	public static Color Thistle
	{
		get
		{
	 		return (Color.intColor(216,191,216,255));
		}
	}
	public static Color LemonChiffon
	{
		get
		{
	 		return (Color.intColor(255,250,205,255));
		}
	}
	public static Color Tomato
	{
		get
		{
	 		return (Color.intColor(253,99,71,255));
		}
	}
	public static Color LightBlue
	{
		get
		{
	 		return (Color.intColor(173,216,230,255));
		}
	}
	public static Color Turquoise
	{
		get
		{
	 		return (Color.intColor(64,224,208,255));
		}
	}
	public static Color LightCoral
	{
		get
		{
	 		return (Color.intColor(240,128,128,255));
		}
	}
	public static Color Violet
	{
		get
		{
	 		return (Color.intColor(238,130,238,255));
		}
	}
	public static Color LightCyan
	{
		get
		{
	 		return (Color.intColor(224,255,255,255));
		}
	}
	public static Color Wheat
	{
		get
		{
	 		return (Color.intColor(245,222,179,255));
		}
	}
	public static Color LightGoldenrodYellow
	{
		get
		{
	 		return (Color.intColor(250,250,210,255));
		}
	}
	public static Color White
	{
		get
		{
	 		return (Color.intColor(255,255,255,255));
		}
	}
	public static Color LightGreen
	{
		get
		{
	 		return (Color.intColor(144,238,144,255));
		}
	}
	public static Color WhiteSmoke
	{
		get
		{
	 		return (Color.intColor(245,245,245,255));
		}
	}
	public static Color LightGray
	{
		get
		{
	 		return (Color.intColor(211,211,211,255));
		}
	}
	public static Color Yellow
	{
		get
		{
	 		return (Color.intColor(255,255,0,255));
		}
	}
	public static Color LightPink
	{
		get
		{
	 		return (Color.intColor(255,182,193,255));
		}
	}
	public static Color YellowGreen
	{

		get{
			return (Color.intColor(154,205,50,255);
		}
	}
}
