﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;

//TextureType.Ambient;
				//TextureType.Diffuse;
				//TextureType.Displacement;
				//TextureType.Emissive;
				//TextureType.Height;
				//TextureType.Lightmap;
				//TextureType.Normals;
				//TextureType.Opacity;
				//TextureType.Reflection;
				//TextureType.Shininess;
				//TextureType.Specular;

namespace OctogonEngine
{
	public class SharedRessource
	{
		public static bool isEnvironnementLoaded = false;
		private static int _index = 0;
		private int _id = 0;
		public static List<SharedRessource> SharedRessources = new List<SharedRessource>();
		public SharedRessource()
		{
			this._id = SharedRessource._index;
			SharedRessource.SharedRessources.Add(this);
			SharedRessource._index++;
		}
	}
	public enum		MaterialType
	{
		standard, shader, mixed
	}
	public class Material : SharedRessource
	{
		private Color4 FromColor(Assimp.Color4D color)
		{
			Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}

		private Color4 FromColor(Color color)
		{
			Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}
		public Texture	diffuseMap = null;
		public Texture	normalMap = null;
		public Texture	alphaMap = null;
		public Texture	specularMap = null;
		public Texture	reflectiveMap = null;
		public Texture	emissiveMap = null;
		public Color4	diffuseColor = new Color4(0.02f, .02f, .02f, 1.0f);
		public Color4	specularColor = new Color4(0, 0, 0, 1.0f);
		public Color4	ambientColor = new Color4(.2f, .2f, .2f, 1.0f);
		public Color4	emissiveColor = new Color4(0, 0, 0, 1.0f);
		public float	shininess = 12f;
		public float	strength = 1f;
		public MaterialType type;
		public void Bind()
		{
			if (diffuseMap != null)
			{
				GL.Enable(EnableCap.Texture2D);
				diffuseMap.Bind();
			}
			else
			{
				GL.BindTexture(TextureTarget.Texture2D, 0);
				GL.Disable(EnableCap.Texture2D);
			}
			//GL.Enable(EnableCap.ColorMaterial);
			if (diffuseColor.A != 1)
			{
				Debug.Log("TRANSPARENCY");
					GL.Enable(EnableCap.Blend);
					GL.DepthMask(false);
			}
			else
			{
				//Debug.Log("NON");
				GL.Disable(EnableCap.Blend);
				GL.DepthMask(true);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, diffuseColor);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Specular, specularColor);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, ambientColor);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Emission, emissiveColor);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Shininess, shininess * strength);
		}
		public void UnBind()
		{
			this.diffuseMap.UnBind();
		}
		public Material(Assimp.Material mat)
		{
				TextureSlot tex;
				if (mat.GetMaterialTexture(TextureType.Diffuse, 0, out tex))
				{
					diffuseMap = new Texture(tex.FilePath);
				}

				if (mat.HasColorDiffuse)
				{
					diffuseColor = FromColor(mat.ColorDiffuse);
				}
				
				if (mat.HasColorSpecular)
				{
					specularColor = FromColor(mat.ColorSpecular);
				}

				if (mat.HasColorAmbient)
				{
					ambientColor = FromColor(mat.ColorAmbient);
				}

				if (mat.HasColorEmissive)
				{
					emissiveColor = FromColor(mat.ColorEmissive);
				}

				shininess = 1;
				strength = 1;
				if (mat.HasShininess)
				{
					shininess = mat.Shininess;
				}
				if (mat.HasShininessStrength)
				{
					strength = mat.ShininessStrength;
				}
		}
	}
	public class Texture : SharedRessource
	{
		public static List<Texture> SharedTextures = new List<Texture>();
		public String path;
		private bool _loaded = false;
		public bool loaded
		{
			get
			{
				return (this._loaded);
			}
			private set
			{
				this._loaded = value;
			}

		}
		public void Reload()
		{
			this.Delete();
			this.LoadFrom(path);
		}
		public void Delete()
		{
			if (loaded)
			{
				GL.DeleteTexture(this._glID);
				loaded = false;
			}
		}
		private int _glID;
		public int glID
		{
			get
			{
				if (!loaded)
					Reload();
				if (!loaded)
					return (-1);
				return (this._glID);
			}
			private set
			{
				this._glID = value;
			}
		}
		public Texture(String path)
		{
			SharedTextures.Add(this);
			this.LoadFrom(path);
		}
		public Texture(int glID)
		{
			this._glID = glID;
		}

		private void LoadFrom(String fileName)
		{
			/*for (int i = 0; i < Texture.SharedTextures.Count; i++)
			{
				if (Texture.SharedTextures[i].path == this.path)
				{
					if (!Texture.SharedTextures[i].loaded)
					{
						Texture.SharedTextures[i].Reload();
						this.path = Texture.SharedTextures[i].path;
						this._glID = Texture.SharedTextures[i]._glID;
						this.loaded = Texture.SharedTextures[i].loaded;
						return ;
					}
				}
			}*/
			
			this.path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
			//this.path = fileName;
			if (!File.Exists(fileName) || !SharedRessource.isEnvironnementLoaded)
			{
				loaded = false;
				return;
			}
			else
			{
				GL.Enable(EnableCap.Texture2D);
				Bitmap textureBitmap = new Bitmap(fileName);
				BitmapData TextureData =
						textureBitmap.LockBits(
						new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
						System.Drawing.Imaging.ImageLockMode.ReadOnly,
						System.Drawing.Imaging.PixelFormat.Format24bppRgb
					);
				int _glID = GL.GenTexture();
				GL.BindTexture(TextureTarget.Texture2D, _glID);
				this._glID = _glID;
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, textureBitmap.Width, textureBitmap.Height, 0,
					OpenTK.Graphics.OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, TextureData.Scan0);
				textureBitmap.UnlockBits(TextureData);

				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				//GL.BindTexture(TextureTarget.Texture2D, 0);
				loaded = true;
			}
			GL.Disable(EnableCap.Texture2D);
		}
		public void Bind()
		{
			if (!loaded)
				this.Reload();
			if (loaded)
				GL.BindTexture(TextureTarget.Texture2D, this._glID);
		}
		public void UnBind()
		{
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}
	}
}
