﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using OctogonEngine;

/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

namespace OctogonEngine
{
	public class StandardExtensions
	{
		public static bool IsNumber(object value)
		{
			return value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}
	}
	public static class Extensions
	{
		public static float square(this float nb)
		{
			float res = nb * nb;
			return (res);
		}
		public static bool HasMethod(this object objectToCheck, string methodName)
		{
			var type = objectToCheck.GetType();
			return type.GetMethod(methodName) != null;
		}
		public static bool IsNumber(this object value)
		{
			return value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}

		public static List<ObjectGroup> getObjectProperties(this object obj)
		{
			return (UserScript.GetProperties(obj));
		}
		public static bool IsValueType<T>(this T obj)
		{
			return default(T) != null;
		}
		

		public static List<ObjectGroup> serialize(this object obj)
		{
			return (UserScript.GetProperties(obj));
		}
		

		public static object GetPropValue(this object src, string propName)
		{
			return (UserScript.GetPropValue(src, propName));
//			return src.GetType().GetProperty(propName).GetValue(src, null);
		}

		public static bool SetPropValue(this object src, string propName, object value)
		{
			return (UserScript.SetPropValue(src, propName, value));
			//			return src.GetType().GetProperty(propName).GetValue(src, null);
		}

		public static IList<T> Clone<T>(this IList<T> listToClone)
		{
			return listToClone.Select(item => (T)item).ToList();
		}
		public static bool TryCast<T>(this object obj)
		{
			if (obj is T)
			{
				return true;
			}
			return false;
		}
		public static bool TryCast<T>(this object obj, out T result)
		{
			if (obj is T)
			{
				result = (T)obj;
				return true;
			}
			result = default(T);
			return false;
		}
		public static float tan(this float nb)
		{
			return ((float)Math.Tan(nb));
		}
		public static float sqrt(this float nb)
		{
			return ((float)Mathf.Sqrt(nb));
		}
		public static object CloneObject(this object o)
		{
			Type t = o.GetType();
			PropertyInfo[] properties = t.GetProperties();


			Object p = t.InvokeMember("", System.Reflection.
				BindingFlags.CreateInstance, null, o, null);


			foreach (PropertyInfo pi in properties)
			{
				if (pi.CanWrite)
				{
					pi.SetValue(p, pi.GetValue(o, null), null);
				}
			}
			return p;
		}
		public static float toDeg(this float angl)
		{
			return ((float)((angl / (2 * Math.PI)) * 360));
		}
		public static float toRad(this float angl)
		{
			return ((float)((angl / 360) * (2 * Math.PI)));
		}
		public static double toDeg(this double angl)
		{
			return ((double)((angl / (2 * Math.PI)) * 360));
		}
		public static double toRad(this double angl)
		{
			return ((float)((angl / 360) * (2 * Math.PI)));
		}
		public static float toFloat(this bool bl)
		{
			if (bl == false)
			{
				return (-1);
			}
			return (1);
		}
		public static bool sign(this float nb)
		{
			if (nb >= 0)
				return (true);
			else
				return (false);
		}
	}
}
