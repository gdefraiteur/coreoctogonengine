﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OctogonEngine
{
	public class Mathf
	{
		public static readonly double epsilon = 1e-6;
		public static double PI = 3.14159265359;
		public static double Square(double nb)
		{
			double res = nb * nb;
			return (res);
		}
		public static double Sqrt(double nb)
		{
			double res = 0;
			double inc = 0.0001;
			while (res * res < nb)
			{
				res += inc;
			}
			return (res);
		}
		public static Vector3 intersectPlane(Vector3 planeNormal, Vector3 planeOrigin, Vector3 lineOrigin, Vector3 lineDirection)
		{
			float t = 0;
			// assuming vectors are all normalized
			planeNormal = planeNormal.normalized;
			lineDirection = lineDirection.normalized;
			float denom = Vector3.Dot(planeNormal, lineDirection);
			if (denom > epsilon)
			{
				Vector3 planeOriginlineOrigin = planeOrigin - lineOrigin;
				t = Vector3.Dot(planeOriginlineOrigin, planeNormal) / denom; 
				return (new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			return (Vector3.zero);
		}

		public static Vector3 intersectPlane(Plane plane, Vector3 lineOrigin, Vector3 lineDirection)
		{
			float t = 0;
			Vector3 planeOrigin = plane.origin;
			// assuming vectors are all normalized
			planeNormal = Plane.normal.normalized;
			lineDirection = lineDirection.normalized;
			float denom = Vector3.Dot(planeNormal, lineDirection);
			if (denom > epsilon)
			{
				Vector3 planeOriginlineOrigin = planeOrigin - lineOrigin;
				t = Vector3.Dot(planeOriginlineOrigin, planeNormal) / denom; 
				return (new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			return (Vector3.zero);
		}


		public static Vector3 intersectPlane(Plane plane, Ray ray)
		{
			float t = 0;
			Vector3 planeOrigin = plane.origin;
			Vector3 lineOrigin = ray.origin;
			// assuming vectors are all normalized
			planeNormal = Plane.normal.normalized;
			lineDirection = ray.direction;
			float denom = Vector3.Dot(planeNormal, lineDirection);
			if (denom > epsilon)
			{
				Vector3 planeOriginlineOrigin = planeOrigin - lineOrigin;
				t = Vector3.Dot(planeOriginlineOrigin, planeNormal) / denom; 
				return (new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			return (Vector3.zero);
		}
		public static Vector3 intersectPlane(Vector3 planeNormal, Vector3 planeOrigin, Ray ray)
		{
			float t = 0;
			// assuming vectors are all normalized
			Vector3 lineOrigin = ray.origin;
			planeNormal = planeNormal.normalized;
			lineDirection = ray.direction.normalized;
			float denom = Vector3.Dot(planeNormal, lineDirection);
			if (denom > epsilon)
			{
				Vector3 planeOriginlineOrigin = planeOrigin - lineOrigin;
				t = Vector3.Dot(planeOriginlineOrigin, planeNormal) / denom; 
				return (new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			return (Vector3.zero);
		}

		public static List<Vector3> intersectSphere(Vector3 center, float radius, Vector3 lineOrigin, Vector3 lineDirection)
		{
			List<Vector3> res = new List<Vector3>();
			float determinant = (lineDirection.Dot(lineOrigin - center)).square() - (lineOrigin - center).magnitude.square() + radius.square();
			float t = - (lineDirection.Dot(lineOrigin - center));
			if (determinant < 0)
			{
				return (null);
			}
			else if (determinant == 0)
			{
				t = t + determinant.sqrt();
				res.Add(new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			if (determinant > 0)
			{
				t = t - determinant.sqrt();
				res.Add(new Vector3(lineOrigin.x + (lineDirection.x * t), lineOrigin.y + (lineDirection.y * t), lineOrigin.z + (lineDirection.z * t)));
			}
			return (res);
		}
	}
}
