﻿/*

Ce projet appartient au groupe Octogon

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

beware ur ass if u do stupid things.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace OctogonEngine
{
	
	public class ObjectGroup
	{
		public string name;
		public dynamic value;
		public ObjectGroup(string name, dynamic value)
		{
			this.name = name;
			this.value = value;
		}

	}
	public class TransformCollection
	{
		private List<Transform> _children;
		private Transform _container;
		public int Count
		{
			get
			{
				return (_children.Count);
			}
		}
		public Transform this[int index]
		{
			get
			{
				return (this._children[index]);
			}
			set
			{
				this._children[index].parent = null;
				this._children[index] = value;
				this._children[index].parent = this._container;
			}
		}
		public void Add(Transform tr)
		{
			this._children.Add(tr);
			if (tr.parent != _container)
				tr.parent = _container;
		}
		public bool Contains(Transform tr)
		{
			for (int i = 0; i < this._children.Count; i++)
			{
				if (this._children[i] == tr)
				{
					return (true);
				}
			}
			return (false);
		}
		public void Remove(Transform tr)
		{

			if (this.Contains(tr))
			{
				tr.parent = null;
			}
			this._children.Remove(tr);
		}
		public void RemoveOnly(Transform tr)
		{
			this._children.Remove(tr);
		}
		public TransformCollection(Transform container)
		{
			this._children = new List<Transform>();
			this._container = container;
			container.children = this;
		}
	}
}
