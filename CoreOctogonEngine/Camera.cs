﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

/*
Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using OctogonEngine;
using GL = OpenTK.Graphics.OpenGL.GL;
namespace OctogonEngine
{
	public struct Rect
	{
		private float _x;
		private float _y;
		private float _xMax;
		private float _yMax;
		public float x
		{
			get
			{
				return (this._x);
			}
			set
			{
				this._x = value;
			}
		}
		public float y
		{
			get
			{
				return (this._y);
			}
			set
			{
				this._y = value;
			}
		}
		public float xMax
		{
			get
			{
				return (this._xMax);
			}
			set
			{
				this._xMax = value;
			}
		}
		public float yMax
		{
			get
			{
				return (this._yMax);
			}
			set
			{
				this._yMax = value;
			}
		}
		public Rect(float x, float y, float xMax, float yMax)
		{
			this._x = x;
			this._y = y;
			this._yMax = yMax;
			this._xMax = xMax;
		}

	}

	public class Camera : MonoBehaviour
	{
		public static readonly List<Camera> Cameras = new List<Camera>();
		private float _nearClipPlane;
		private float _farClipPlane;
		private static Camera _main = null;
		public static Camera main
		{
			get
			{
				if (_main == null)
				{
					if (Cameras.Count > 0)
					{
						_main = Cameras[0];
					}
				}
				return (_main); 
			}
			set
			{
				_main = value;
			}
		}

		private void updatePerspective()
		{
			if (this._orhtographic)
			{
				this._perspectiveMatrix = Matrix.Orthographic(0, 0, this.width, this.height, this._nearClipPlane, this._farClipPlane);
			}
			else
			{
				this._perspectiveMatrix = Matrix.Projection(this._fov, this.aspectRatio, this._nearClipPlane, this._farClipPlane);
			}
		}

		public Rect pixelRect;
		private Matrix _perspectiveMatrix;
		public Matrix perspectiveMatrix
		{
			get
			{
				return (this._perspectiveMatrix);
			}
			set
			{
				if (value.width == 4 && value.height == 4)
					this._perspectiveMatrix = value;
			}
		}
		public float nearClipPlane
		{
			get
			{
				return (this._nearClipPlane);
			}
			set
			{
				this._nearClipPlane = value;
				this.updatePerspective();
			}
		}
		public float farClipPlane
		{
			get
			{
				return (this._farClipPlane);
			}
			set
			{
				this._farClipPlane = value;
				this.updatePerspective();
			}
		}

		private bool _orhtographic;
		public bool orthographic
		{
			get
			{
				return (this._orhtographic);
			}
			set
			{
				this._orhtographic = value;
				this.updatePerspective();
			}

		}
		private float _fov;
		public float fov
		{
			get
			{
				return (this._fov);
			}
			set
			{
				this._fov = value;
				this.updatePerspective();

			}
		}
		private int _width;
		public int width
		{
			get
			{
				return (this._width);
			}
			set
			{
				this._width = value;
				this.updatePerspective();
			}
		}
		public float aspectRatio
		{
			get
			{
				return ((float)this._width / (float)this._height);
			}
			set
			{
				;
			}
		}
		private int _height;
		public int height
		{
			get
			{
				return (this._height);
			}
			set
			{
				this._height = value;
				this.updatePerspective();
			}
		}

		public void PickMatrix(double x, double y, double deltax, double deltay, int[] viewport)
		{
			if (deltax <= 0 || deltay <= 0)
			{ 
				return;
			}
			GL.Translate((viewport[2] - 2 * ((x + deltax) - viewport[0])) / deltax,
				(viewport[3] - 2 * ((y) - viewport[1])) / deltay, 0);
			GL.Scale(viewport[2] / deltax, viewport[3] / deltay, 1.0);
		}

		public Transform GetSelection(int mouseX, int mouseY, GameWindow game)
		{

			float sw = 1;
			float sh = 1;
			mouseX = mouseX - 1;
			int[] selectBuffer = new int[1024];
			int hits;
			int[] viewport = new int[4];
			//GL.GetInteger(GetPName.Viewport, viewport);
			viewport = new int[]{0, 0, game.Width, game.Height};
			GL.Viewport(0, 0, game.Width, game.Height);
			GL.SelectBuffer(1024, selectBuffer);
			GL.RenderMode(OpenTK.Graphics.OpenGL.RenderingMode.Select);
			GL.InitNames();
			float aspectRatio = game.Width / (float)game.Height;
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, 1, 1000);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			PickMatrix(mouseX, viewport[3] - mouseY, sw, sh, viewport);
			GL.MultMatrix(ref perspective);

			((dynamic)game).OnRenderFrame();
			((dynamic)game).SwapBuffers();
			GL.Flush();
			float distance = -1;
			int selectedIndex = -1;
			hits = GL.RenderMode(RenderingMode.Render);
			if (hits != 0)
			{
				for (int n = 0; n < hits; n++)
            	{
					uint newDist = (uint)selectBuffer[n * 4 + 1];
					if (distance >= newDist || distance == -1)
					{
						distance = newDist;
						selectedIndex = selectBuffer[n * 4 + 3];
					}
            	}
			}
			if (distance != -1)
			{
				return (((Transform)(MonoBehaviour.MonoBehaviours[selectedIndex])));
			}

			GL.PopName();
			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
			return (null);
		}

		public List<Transform> GetSelectionList(int mouseX, int mouseY, GameWindow game)
		{
			float sw = 1;
			float sh = 1;
			mouseX = mouseX - 1;
			int[] selectBuffer = new int[1024];
			int hits;
			int[] viewport = new int[4];
			GL.GetInteger(GetPName.Viewport, viewport);
			GL.Viewport(0, 0, game.Width, game.Height);
			GL.SelectBuffer(1024, selectBuffer);
			GL.RenderMode(RenderingMode.Select);
			
			GL.InitNames();
			
			float aspectRatio = game.Width / (float)game.Height;
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, 1, 1000);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			PickMatrix(mouseX, viewport[3] - mouseY, sw, sh, viewport);
			GL.MultMatrix(ref perspective);

			((dynamic)game).OnRenderFrame();
			((dynamic)game).SwapBuffers();
			

			GL.Flush();
			hits = GL.RenderMode(RenderingMode.Render);
			List<Transform> res = new List<Transform>();
			if (hits != 0)
			{
				for (int n = 0; n < hits; n++)
            	{
					//uint newDist = (uint)selectBuffer[n * 4 + 1];
					res.Add((Transform)MonoBehaviour.MonoBehaviours[selectBuffer[n * 4 + 3]]);
            	}
			}


			GL.PopName();
			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
			return (res);
		}

		public Ray ScreenPointToRay(Vector2 screenPosition)
		{
			float x = screenPosition.x;
			float y = screenPosition.y;
			float dx = ((x / (this.width / 2)) -1.0f)/this.aspectRatio;
			if (!this.orthographic)
				dx *= (float)Math.Tan(this.fov * 0.5f);
			float dy = 1.0f - (y / (this.height / 2));
			if (!this.orthographic)
				dy *= (float)Math.Tan(this.fov * 0.5f);
			Vector3 p1 = new Vector3(dx * this.nearClipPlane, dy * this.nearClipPlane, this.nearClipPlane);
			Vector3 p2 = new Vector3(dx * this.farClipPlane, dy * this.farClipPlane, this.farClipPlane);
			return (new Ray(p1, p2));
		}
		public Vector3 WorldToCameraPosition(Vector3 position)
		{
			Vector3 localPos = this.transform.inverseTransformPoint(position);
			Vector3 screenPos = localPos * this.perspectiveMatrix;

			Vector2 p1 = new Vector2((screenPos.x / screenPos.w), (screenPos.y / screenPos.w));// , dy * this.nearClipPlane, this.nearClipPlane);
			//Vector3 p2 = new Vector3(dx * this.farClipPlane, dy * this.farClipPlane, this.farClipPlane);
			//p1 += new Vector2
			Vector3 res = new Vector3((p1.x * this.width) + this.width / 2, (p1.y * this.height) + this.height / 2, screenPos.z);
			return (res);
		}

/*		public Vector2 get2dPoint(Vector3 vector, Matrix viewMatrix, Matrix projectionMatrix, int width, int height)
		{
      			Matrix viewProjectionMatrix = projectionMatrix * viewMatrix;
      			//transform world to clipping coordinates
      			Vector3 point3D = viewProjectionMatrix.multiply(point3D);
      			int winX = (int) Math.round((( point3D[0] + 1 ) / 2.0) *
      			                             width );
      			//we calculate -point3D.getY() because the screen Y axis is
      			//oriented top->down 
      			int winY = (int) Math.round((( 1 - point3D.Y ) / 2.0) *
      			                             height );
      			return (new Vector2(winX, winY));
		}
*/
		/*public Ray screenPointToRay(Vector2 screenPosition)
		{
			float dx = ((x / (this.width / 2)) -1.0f)/this.aspectRatio;
			if (!this.orthographic)
				dx *= Mathf.tan(this.fov * 0.5f);
			float dy = 1.0f - (y / (this.height / 2));
			if (!this.orthographic)
				dy *= tanf(FOV*0.5f);
			Vector3 p1 = new Vector3(dx * this.nearClipPlane, dy * this.nearClipPlane, this.nearClipPlane);
			Vector3 p2 = new Vector3(dx * this.farClipPlane, dy * this.farClipPlane, this.farClipPlane);
			return (new Ray(p1, p2));
		}*/
		public Camera()
		{
			Camera.Cameras.Add(this);
			Console.WriteLine("ok");

			this._width = (int)1024;
			this._height = (int)768;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}

		public Camera(int width, int height)
		{
			this._width = (int)width;
			this._height = (int)height;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}
		public Camera(Vector2 size)
		{
			this._width = (int)size.x;
			this._height = (int)size.y;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}
		public Camera(Vector2 size, float fov, float nearClipPlane, float farClipPlane)
		{
			this._width = (int)size.x;
			this._height = (int)size.y;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = nearClipPlane;
			this._farClipPlane = farClipPlane;
		}
		public Camera(int width, int height, float fov, float nearClipPlane, float farClipPlane)
		{
			this._width = (int)width;
			this._height = (int)height;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = nearClipPlane;
			this._farClipPlane = farClipPlane;
		}

		//(MathHelper.PiOver4, aspectRatio, 1, 64);
	}
}
