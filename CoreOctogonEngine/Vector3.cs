﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Runtime.InteropServices;

namespace OctogonEngine
{
	public struct Size
	{
		private float[] _array = new float[2];
		public float[] array
		{
			get
			{
				return (this._array.Clone());
			}
			private set
			{
				this._array = value;
			}
		}
		public float WIDTH
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float HEIGHT
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float Width
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float Height
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float w
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float h
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float W
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float H
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float width
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float height
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float x
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}

		public float X
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}

		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}


		public Size(float x, float y)
		{
			this[0] = x;
			this[1] = y;
		}
		public static float Dot(Size v1, Size v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y));
		}
		public static Size Cross(Size v1, Size v2)
		{
			Size res = new Size((v1.x * v2.y) - (v1.y * v2.x), (v1.y * v2.x) - (v1.x * v2.y));
			return (res);
		}

		public override bool Equals(object other)
		{
			return ((!(other == null)) && (Vector3)other.GetHashCode() == this.GetHashCode());
		}
		public override int GetHashCode()
		{
			int hash = 0;
			for (int i = 0; i < this._array.Length; i++)
			{
				hash += (this._array[i] * i * 5) + 21;
			}
			return (hash);
		}
		Size(Size copy)
		{
			this._x = copy.x;
			this._y = copy.y;
		}
		public static Size operator -(Size vector)
		{
			return (new Size(-vector.x, -vector.y));
		}
		public static Size operator -(Size vector, Size other)
		{
			return (new Size(vector.x - other.x, vector.y - other.y));
		}
		public override string ToString()
		{
			return ("Size(" + this.x + ", " + this.y + ");\n");
		}

		private float _x;
		private float _y;
		public static Size zero
		{
			get
			{
				return (new Size(0, 0));
			}
		}
		public static Size one
		{
			get
			{
				return (new Size(1, 1));
			}
		}
		public static Size up
		{
			get
			{
				return (new Size(0, 1));
			}
		}
		public static Size right
		{
			get
			{
				return (new Size(1, 0));
			}
		}
		public static bool operator ==(Size self, Size other)
		{
			return (self.x == other.x && self.y == other.y);
		}
		public static bool operator !=(Size self, Size other)
		{
			return (!(self == other));
		}
	}

	public struct Vector2
	{
		private float[] _array = new float[2];
		public float[] array
		{
			get
			{
				return (this._array.Clone());
			}
			private set
			{
				this._array = value;
			}
		}
		public float WIDTH
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float HEIGHT
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float Width
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float Height
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float w
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float h
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float W
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float H
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float width
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float height
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float x
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}

		public float X
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}

		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}


		public Vector2(float x, float y)
		{
			this[0] = x;
			this[1] = y;
		}
		public static float Dot(Vector2 v1, Vector2 v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y));
		}
		public static Vector2 Cross(Vector2 v1, Vector2 v2)
		{
			Vector2 res = new Vector2((v1.x * v2.y) - (v1.y * v2.x), (v1.y * v2.x) - (v1.x * v2.y));
			return (res);
		}

		public override bool Equals(object other)
		{
			return ((!(other == null)) && (Vector3)other.GetHashCode() == this.GetHashCode());
		}
		public override int GetHashCode()
		{
			int hash = 0;
			for (int i = 0; i < this._array.Length; i++)
			{
				hash += (this._array[i] * i * 5) + 21;
			}
			return (hash);
		}
		Vector2(Vector2 copy)
		{
			this._x = copy.x;
			this._y = copy.y;
		}
		public static Vector2 operator -(Vector2 vector)
		{
			return (new Vector2(-vector.x, -vector.y));
		}
		public static Vector2 operator -(Vector2 vector, Vector2 other)
		{
			return (new Vector2(vector.x - other.x, vector.y - other.y));
		}
		public override string ToString()
		{
			return ("Vector2(" + this.x + ", " + this.y + ");\n");
		}

		private float _x;
		private float _y;
		public static Vector2 zero
		{
			get
			{
				return (new Vector2(0, 0));
			}
		}
		public static Vector2 one
		{
			get
			{
				return (new Vector2(1, 1));
			}
		}
		public static Vector2 up
		{
			get
			{
				return (new Vector2(0, 1));
			}
		}
		public static Vector2 right
		{
			get
			{
				return (new Vector2(1, 0));
			}
		}
		public static bool operator ==(Vector2 self, Vector2 other)
		{
			return (self.x == other.x && self.y == other.y);
		}
		public static bool operator !=(Vector2 self, Vector2 other)
		{
			return (!(self == other));
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct Vector3
	{
		public float GetDist(Vector3 other)
		{
			return ((other - this).Length());
		}
		public Vector3(Assimp.Vector3D other)
		{
			this._array = new float[4];
			this._array[0] = other.X;
			this._array[1] = other.Y;
			this._array[2] = other.Z;
			this._array[3] = 1;
		}
		public Vector3 normalized
		{
			get
			{
				if (this.magnitude == 0)
					return (Vector3.zero);
				return (new Vector3(this.x / this.magnitude, this.y / this.magnitude, this.z / this.magnitude));
			}
		}

		public override bool Equals(object other)
		{
			return ((!(other == null)) && (Vector3)other.GetHashCode() == this.GetHashCode());
		}
		public override int GetHashCode()
		{
			int hash = 0;
			for (int i = 0; i < this._array.Length; i++)
			{
				hash += (this._array[i] * i * 5) + 21;
			}
			return (hash);
		}
		public override string ToString()
		{
			return ("Vector3(" + this.x + ", " + this.y + ", " + this.z + ");\n");
		}
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square() + this.z.square()).sqrt());
			}
		}
		public float x
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public static bool operator== (Vector3 self, Vector3 other)
		{
			if (self.x == other.x && self.y == other.y && self.z == other.z)
			{
				return (true);
			}
			return (false);
		}
		public static bool operator!= (Vector3 self, Vector3 other)
		{
			return (!(self == other));
		}
		public float y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}

		public float X
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float Z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float W
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}


		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public Vector3(float x, float y, float z)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public Vector3(float x, float y, float z, float w)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public float[] array
		{
			get
			{
				return ((float[])this._array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		private float[] _array;
		public static Vector3 up
		{
			get
			{
				return(new Vector3(0, 1, 0));
			}
		}
		public static Vector3 right
		{
			get
			{
				return (new Vector3(1, 0, 0));
			}
		}
		public static Vector3 forward
		{
			get
			{
				return (new Vector3(0, 0, 1));
			}
		}
		public static Vector3 one
		{
			get
			{
				return (new Vector3(1, 1, 1));
			}
		}
		public static Vector3 zero
		{
			get
			{
				return (new Vector3(0, 0, 0));
			}
		}
		public Vector3(Vector3 copy)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = copy.x;
			this.y = copy.y;
			this.z = copy.z;
			this.w = copy.w;
		}
		public Vector3 Cross(Vector3 other)
		{
			return (Vector3.Cross(this, other));
		}
		public static Vector3 Cross(Vector3 v1, Vector3 v2)
		{
			Vector3 tmp = new Vector3((v1.y * v2.z) - (v2.y * v1.z), (v1.z * v2.x) - (v2.z * v1.x), (v1.x * v2.y) - (v2.x * v1.y));
			return ((Vector3)(tmp));
		}
		public float Length()
		{
			return ((float)Mathf.Sqrt(Mathf.Square(this.x) + Mathf.Square(this.y) + Mathf.Square(this.z)));
		}
		public static Vector3 Normalize(Vector3 vector)
		{
			return (vector / vector.Length());
		}
		public static Vector3 operator-(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x - other.x, self.y - other.y, self.z - other.z));
		}
		public static Vector3 operator*(Vector3 self, float other)
		{
			return (new Vector3(self.x * other, self.y * other, self.z * other));
		}
		public static Vector3 operator /(Vector3 self, float other)
		{
			return (new Vector3(self.x / other, self.y / other, self.z / other));
		}
		public static Vector3 operator *(float other, Vector3 self)
		{
			return (new Vector3(self.x * other, self.y * other, self.z * other));
		}
		public static Vector3 operator *(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x * other.x, self.y * other.y, self.z * other.z));
		}
		public static Vector3 operator /(Vector3 self, Vector3 other)
		{
			Vector3 rectified = other;

			if (other.x == 0)
			{
				rectified.x = 0.0001f;
			}
			if (other.y == 0)
			{
				rectified.y = 0.0001f;
			}
			if (other.z == 0)
			{
				rectified.z = 0.0001f;
			}
			return (new Vector3(self.x / other.x, self.y / other.y, self.z / other.z));
		}
		public static Vector3 operator *(Vector3 self, Matrix other)
		{
			return (other * self);
		}
		public static Vector3 operator * (Vector3 self, Quaternion other)
		{
			return (other * self);
		}
		public static Vector3 operator+(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x + other.x, self.y + other.y, self.z + other.z));
		}

		public float Dot(Vector3 v2)
		{
			return ((this.x * v2.x) + (this.y * v2.y) + (this.z * v2.z));
		}
		public static float Dot(Vector3 v1, Vector3 v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z));
		}
		
	}
};
