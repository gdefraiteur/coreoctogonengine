#!/usr/bin/python
import sys
from os import listdir
from os.path import isfile, join
import subprocess

compilationArgs = ["mcs"]
assemblies = []
csfiles = []
for arg in sys.argv[1:] :
	if (".cs" in arg):
		csfiles.append(arg)
	if ("-R:" in arg) :
		split = ""
		split += arg.split(":")[1]
		for filename in listdir(split):
			if (".dll" in filename and not ".config" in filename):
				assemblies.append(split + "/" + filename)
	if ("-r:" in arg):
		compilationArgs.append(arg)


for file in csfiles:
	compilationArgs.append(file) 
#compilationArgs.extend(csfiles)
print ("files:")
print (compilationArgs)
for assembly in assemblies:
	compilationArgs.append("-r:" + assembly) 
compilationArgs.append("-r:" + "System.Drawing.dll")
compilationArgs.append("-r:" + "System.Windows.dll")
compilationArgs.append("-r:" + "mscorlib.dll")
compilationArgs.append("-r:" + "System.Data.dll")
compilationArgs.append("-r:" + "System.Core.dll")
compilationArgs.append("-out:OctogonEngine.dll")
compilationArgs.append("-target:" + "library")
#compilationArgs.append("-r:" + "System.Threading.dll")
print (",\n+ assemblies: ")
print(compilationArgs)
p2 = subprocess.Popen(["brew", "ls", "--versions", "mono"], stdout = subprocess.PIPE)
out1, err1 = p2.communicate()
if ("mono 4.0.1" in out1) :
	print ("ok")
else :
	p3 = subprocess.Popen(["brew", "install", "mono"], stdout=subprocess.PIPE)
	print(out1)
p = subprocess.Popen(compilationArgs, stdout=subprocess.PIPE)
out, err = p.communicate()
print(out)
print(err)

subprocess.call(compilationArgs)