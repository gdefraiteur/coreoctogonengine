﻿/*

Ce projet appartient au groupe OctogonEngine

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OctogonEngine;

namespace OctogonEngine
{
	public struct Quaternion
	{
		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public float x
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this[2]);
			}
			set
			{
				this[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this[3]);
			}
			set
			{
				this[3] = value;
			}
		}
		public static Quaternion identity
		{
			get
			{
				return (new Quaternion(0, 0, 0, 1));
			}
		}

		public Quaternion(Quaternion copy)
		{
			this._array = (float[])copy.array.Clone();
		}
		public Quaternion(Vector3 eulerRepresentation)
		{
			this._array = identity.array;
			this.eulerAngles = new Vector3(eulerRepresentation.x, eulerRepresentation.y, eulerRepresentation.z);// eulerRepresentation;
		}
		public Quaternion(float x, float y, float z, float w)
		{
			this._array = new float[] { x, y, z, w };
		}
		public Quaternion(Vector3 xyz, float w)
		{
			this._array = new float[] { xyz.x, xyz.y, xyz.z, w };
		}
		/*public Quaternion(Vector4 vector)
		{
			this._array = vector.array;
		}*/
		private float[] _array;
		public float[] array
		{
			get
			{
				return ((float[])_array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		public Vector3 eulerAngles
		{
			get
			{
				double yaw = Math.Atan2((2 * ((this[3] * this[0]) + (this[1] * this[2]))), 1 - (2 * (Mathf.Square(this[0]) + Mathf.Square(this[1]))));
				double pitch = Math.Asin(2 * ((this[3] * this[1]) - (this[2] * this[0])));
				double roll = Math.Atan2(2 * ((this[3] * this[2]) + (this[0] * this[1])), 1 - (2 * (Mathf.Square(this[1]) + Mathf.Square(this[2]))));
				return (new Vector3((float)yaw.toDeg(), (float)pitch.toDeg(), (float)roll.toDeg()));
			}
			set
			{

				double w = (Math.Cos(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) + (Math.Sin(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double x = (Math.Sin(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) - (Math.Cos(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double y = (Math.Cos(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) + (Math.Sin(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double z = (Math.Cos(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2)) - (Math.Sin(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2));
				this.x = (float)x;
				this.y = (float)y;
				this.z = (float)z;
				this.w = (float)w;
			}
		}
		public Matrix orthogonalMatrix
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				res.setRow(0, new double[] { Mathf.Square(this.w) + Mathf.Square(this.x) - Mathf.Square(this.y) - Mathf.Square(this.z), (2 * this.x * this.y) - (2 * this.w * this.z), 2 * (this.w * this.y) + (2 * (this.x * this.z)), 0 });
				res.setRow(1, new double[] { 2 * this.w * this.z + 2 * this.x * this.y, Mathf.Square(this.w) - Mathf.Square(this.x) + Mathf.Square(this.y) - Mathf.Square(this.z), 2 * this.y * this.z - 2 * this.w * this.x, 0 });
				res.setRow(2, new double[] { 2 * this.x * this.z - 2 * this.w * this.y, 2 * this.w * this.x + 2 * this.y * this.z, Mathf.Square(this.w) - Mathf.Square(this.x) - Mathf.Square(this.y) + Mathf.Square(this.z), 0 });
				res.setRow(3, new double[] { 0, 0, 0, 1 });
				return (res);
			}
		}
		public Vector3 vector
		{
			get
			{
				return (new Vector3(this.x, this.y, this.z));
			}
			set
			{
				this.x = value.x;
				this.y = value.y;
				this.z = value.z;
			}
		}
		public static Quaternion scalar(float nb)
		{
			Quaternion tmp = Quaternion.identity;
			return (new Quaternion(0, 0, 0, nb));
		}
		public static Quaternion i
		{
			get
			{
				return (new Quaternion(1, 0, 0, 0));
			}
		}
		public static Quaternion j
		{
			get
			{
				return (new Quaternion(0, 1, 0, 0));
			}
		}
		public static Quaternion k
		{
			get
			{
				return (new Quaternion(0, 0, 1, 0));
			}
		}

		public float modulus
		{
			get
			{
				Quaternion conjugated = this * this.conjugate();
				return ((float)Mathf.Sqrt(conjugated.x.square() + conjugated.y.square() + conjugated.z.square() + conjugated.w.square()));
			}
		}
		public static Quaternion operator *(Quaternion Q1, Quaternion Q2)
		{

			float w = (Q1.w * Q2.w) - (Q1.x * Q2.x) - (Q1.y * Q2.y) - (Q1.z * Q2.z);
    		float x = (Q1.w * Q2.x) + (Q1.x * Q2.w) + (Q1.y * Q2.z) - (Q1.z * Q2.y);
    		float y = (Q1.w * Q2.y) - (Q1.x * Q2.z) + (Q1.y * Q2.w) + (Q1.z * Q2.x);
    		float z = (Q1.w * Q2.z) + (Q1.x * Q2.y) - (Q1.y * Q2.x) + (Q1.z * Q2.w);
			return (new Quaternion(x, y, z, w));
		}
		public static Vector3 operator *(Quaternion Q1, Vector3 vector)
		{
			return (Q1.orthogonalMatrix * vector);
			//return (new Quaternion(x, y, z, w));
		}
		public static Vector3 operator /(Quaternion Q1, Vector3 vector)
		{
			return (Q1.inverse * vector);
		}
		public static Quaternion operator +(Quaternion Q1, Quaternion Q2)
		{

			return (new Quaternion(Q1.x + Q2.x, Q1.y + Q2.y, Q1.z + Q2.z, Q1.w + Q2.w));
		}
		public static Quaternion operator -(Quaternion Q1, Quaternion Q2)
		{

			return (new Quaternion(Q1.x - Q2.x, Q1.y - Q2.y, Q1.z - Q2.z, Q1.w - Q2.w));
		}
		public static Quaternion operator /(Quaternion Q1, Quaternion Q2)
		{
			return (Q1.inverse * Q2);
		}
		public static Quaternion operator /(Quaternion self, float nb)
		{
			return (new Quaternion(self.x / nb, self.y / nb, self.z / nb, self.w / nb));
		}
		public double norm()
		{
			return Mathf.Sqrt(this[0]*this[0]+this[1]*this[1]+
                                      this[2]*this[2]+this[3]*this[3]);
		}
		public Quaternion conjugate()
		{
			return (new Quaternion(-this.x, -this.y, -this.z, this.w));  // * ((this + i) * (this) * (i + j) * (this) * (j + k) * (this * k)));
		}
		public Quaternion inverseQuaternion()
		{
			return (this.conjugate() / (float)this.norm());
		}
		public Quaternion inverse
		{
			get
			{
				return (this.inverseQuaternion());
			}
			set
			{

				Quaternion tmp = value.inverseQuaternion();
				this._array = tmp.array;
			}
		}
		public static Quaternion LookAtQuaternion(Vector3 sourcePoint, Vector3 destPoint)
		{
			Vector3 forwardVector = Vector3.Normalize(destPoint - sourcePoint);

			float dot = Vector3.Dot(Vector3.forward, forwardVector);

			if (Math.Abs(dot - (-1.0f)) < 0.000001f)
			{
				return new Quaternion(Vector3.up.x, Vector3.up.y, Vector3.up.z, 3.1415926535897932f);
			}
			if (Math.Abs(dot - (1.0f)) < 0.000001f)
			{
				return Quaternion.identity;
			}

			float rotAngle = (float)Math.Acos(dot);
			Vector3 rotAxis = Vector3.Cross(Vector3.forward, forwardVector);
			rotAxis = Vector3.Normalize(rotAxis);
			return (CreateFromAxisAngle(rotAxis, rotAngle));
		}

		// just in case you need that function also
		public static Quaternion CreateFromAxisAngle(Vector3 axis, float angle)
		{
			float halfAngle = angle * .5f;
			float s = (float)System.Math.Sin(halfAngle);
			Quaternion q = Quaternion.identity;
			q.x = axis.x * s;
			q.y = axis.y * s;
			q.z = axis.z * s;
			q.w = (float)System.Math.Cos(halfAngle);
			return q;
		}
		public Quaternion(Matrix mat)
		{
			Quaternion tmp = mat.quaternion;
			this._array = tmp.array;
		}
		public Quaternion(Assimp.Quaternion other)
		{
			Quaternion tmp = Quaternion.identity;
			this._array = tmp._array;
			this.x = other.X;
			this.y = other.Y;
			this.z = other.Z;
			this.w = other.W;
		}


		public void LookAt(Vector3 sourcePoint, Vector3 destPoint)
		{
			Quaternion tmp = LookAtQuaternion(sourcePoint, destPoint);
			this._array = tmp.array;
		}

		public override string ToString()
		{
			string res = "Quaternion( w: " + this.w + ", " + this.x + ", " + this.y + ", " + this.z + "); \n";
			return res;
		}
	}
}
