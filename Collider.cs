/*
Ce projet appartient au groupe Octogon

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

beware ur ass if u do stupid things.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using Jitter;
using System.Collections.Generic;
using System.Collections;
using Jitter.LinearMath;
using Jitter.Collision.Shapes;
using System;
using Jitter.Collision;
using JRigidBody = Jitter.Collision.RigidBody;
namespace OctogonEngine
{
	public enum PhysicMaterialCombine
	{
		Average,
		Minimum,
		Multiply,
		Maximum
	}

	public class PhysicMaterial : SharedRessources
	{
		public PhysicMaterialCombine bounceCombine = PhysicMaterialCombine.Average;
		public float bounciness = 0;
		public float dynamicFriction = 0;
		public float dynamicFriction2 = 0;
		public PhysicMaterialCombine frictionCombine = PhysicMaterialCombine.Average;
		public float staticFriction = 0;
		public float staticFriction2 = 0;
		public static List<PhysicMaterial> PhysicMaterials = new List<PhysicMaterial>();
		public PhysicMaterial()
		{
			PhysicMaterial.PhysicMaterials.Add(this);
		}
		public PhysicMaterial(float bounciness, float dynamicFriction, float staticFriction, PhysicMaterialCombine frictionCombine)
		{
			this.bounciness = bounciness;
			this.dynamicFriction = dynamicFriction;
			this.staticFriction = staticFriction;
			this.dynamicFriction2 = 0;
			this.staticFriction2 = 0;
			this.frictionCombine = frictionCombine;
		}
		public PhysicMaterial(float bounciness, float dynamicFriction, float staticFriction, float dynamicFriction2, float staticFriction2, PhysicMaterialCombine frictionCombine)
		{
			this.bounciness = bounciness;
			this.dynamicFriction = dynamicFriction;
			this.staticFriction = staticFriction;
			this.dynamicFriction2 = dynamicFriction2;
			this.staticFriction2 = staticFriction2;
			this.frictionCombine = frictionCombine;
		}
	}

	public struct ContactPoint
	{
		public Vector3 normal = Vector3.zero;
		public Collider otherCollider = null;
		public Vector3 point = Vector3.zero;
		public Collider thisCollider = null;
		public ContactPoint(Vector3 normal, Collider otherCollider, Vector3 point, Collider thisCollider)
		{
			this.normal = normal;
			this.otherCollider = otherCollider;
			this.point = point;
			this.thisCollider = thisCollider;
		}
	}

	public struct Collision
	{
		public Collider collider = null;
		public ContactPoint[] contacts = null;
		public GameObject gameObject = null;
		public float relativeVelocity = 0;
		public Rigidbody rigidbody = null;
		public Transform transform = null;
		public Collision(Collider collider, ContactPoint[] contacts, GameObject gameObject, float relativeVelocity, Rigidbody rigidbody, Transform transform)
		{
			this.collider = collider;
			this.contacts = contacts;
			this.gameObject = gameObject;
			this.relativeVelocity = relativeVelocity;
			this.rigidbody = rigidbody;
			this.transform = transform;
		}
	}
/*	
attachedRigidbody	The rigidbody the collider is attached to.
bounds	The world space bounding volume of the collider.
contactOffset	Contact offset value of this collider.
enabled	Enabled Colliders will collide with other colliders, disabled Colliders won't.
isTrigger	Is the collider a trigger?
material	The material used by the collider.
sharedMaterial	The shared physic material of this collider.
gameObject	The game object this component is attached to. A component is always attached to a game object.
tag	The tag of this game object.
transform	The Transform attached to this GameObject (null if there is none attached).
hideFlags	Should the object be hidden, saved with the scene or modifiable by the user?
name
*/
	public class Physic
	{
		public CollisionSystem collisionSystem;
		private void CollisionDetected (RigidBody body1, RigidBody body2, JVector point1, JVector point2, JVector normal, float penetration)
		{
			Debug.Log(" Collision detected !");
		}
	
	}
	/*

angularDrag	The angular drag of the object.
angularVelocity	The angular velocity vector of the rigidbody.
centerOfMass	The center of mass relative to the transform's origin.
collisionDetectionMode	The Rigidbody's collision detection mode.
constraints	Controls which degrees of freedom are allowed for the simulation of this Rigidbody.
detectCollisions	Should collision detection be enabled? (By default always enabled).
drag	The drag of the object.
freezeRotation	Controls whether physics will change the rotation of the object.
inertiaTensor	The diagonal inertia tensor of mass relative to the center of mass.
inertiaTensorRotation	The rotation of the inertia tensor.
interpolation	Interpolation allows you to smooth out the effect of running physics at a fixed frame rate.
isKinematic	Controls whether physics affects the rigidbody.
mass	The mass of the rigidbody.
maxAngularVelocity	The maximimum angular velocity of the rigidbody. (Default 7) range { 0, infinity }.
maxDepenetrationVelocity	Maximum velocity of a rigidbody when moving out of penetrating state.
position	The position of the rigidbody.
rotation	The rotation of the rigdibody.
sleepThreshold	The mass-normalized energy threshold, below which objects start going to sleep.
solverIterationCount	Allows you to override the solver iteration count per rigidbody.
useConeFriction	Force cone friction to be used for this rigidbody.
useGravity	Controls whether gravity affects this rigidbody.
velocity	The velocity vector of the rigidbody.
worldCenterOfMass	
	*/
	public enum CollisionDetectionMode
	{
		Discrete,
		Continuous,
		ContinuousDynamic
	}

	public enum RigidbodyContraints
	{
		None,
		FreezePositionX,
		FreezePositionY,
		FreezePositionZ,
		FreezeRotationX,
		FreezeRotationY,
		FreezeRotationZ,
		FreezePosition,
		FreezeRotation,
		FreezeAll
	}
	public enum RigidbodyInterpolation
	{
		None,
		Interpolate,
		Extrapolate
	}
	public enum ForceMode
	{
		Force,
		Acceleration,
		Impulse,
		VelocityChange
	}

	public class RigidBody: MonoBehaviour
	{
		public float angularDrag;
		public Vector3 angularVelocity;
		public Vector3 centerOfMass;
		public CollisionDetectionMode collisionDetectionMode;
		public RigidbodyContraints contraints;
		public bool detectCollisions;
		public float drag;
		public bool freezeRotation;
		public Vector3 inertiaTensor;
		public Quaternion inertiaTensorRotation;
		public RigidbodyInterpolation interpolation;
		public bool isKinematic;
		public float mass;
		public float maxAngularVelocity;
		public float maxDepenetrationVelocity;
		public Vector3 position;
		public Vector3 rotation;
		public float sleepThreshold;
		public int solverIterationCount;
		public bool useConeFriction;
		public bool useGravity;
		public float velocity;
		public JRigidBody jrigidbody;
		public Vector3 worldCenterOfMass
		{
			get
			{
				return (Vector3.zero);
			}
		}
		public void AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode mode = ForceMode.Force)
		{
			;
		}
		public void AddForce(Vector3 force, ForceMode mode = ForceMode.Force)
		{
			;
		}
		public void AddRelativeTorque(Vector3 torque, ForceMode mode = ForceMode.Force)
		{
			;
		}
		public void AddTorque(Vector3 torque, ForceMode mode = ForceMode.Force)
		{
			;
		}
		public Vector3 ClosestPointOnBounds(Vector3 position)
		{
			float dist = -1;
			Vector3 closest = Vecto3.zero;
			List<MonoBehaviour> colliders = this.GetComponent<Collider>();
			for (int i = 0; i < colliders.Count; i++)
			{
				Vector3 newPoint = colliders[i].ClosestPointOnBounds(position);
				float newDist = newPoint.GetDist(position);
				if (newDist < dist)
				{
					dist = newDist;
					closest = newPoint;
				}
			}
			return (closest);
		}
		public float GetPointVelocity(Vector3 worldPosition)
		{
			return (0);
		}
		public float GetRelativePointVelocity(Vector3 localPosition)
		{
			return (0);
		}
		public bool isSleeping()
		{
			;
		}
		public void MovePosition(Vector3 position)
		{
			this.position = position;
		}
		public void MoveRotation(Quaternion rotation)
		{
			this.rotation = rotation;
		}
		public void SetDensity(float density)
		{
			//genre un lossyscale version masse en somme j imagine
			;
		}
		public void Sleep()
		{
			;
		}
		public bool SweepTest(Vector3 direction, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity)
		{
			;
		}
		public RaycastHit[] SweepTestAll(Vector3 direction, float maxDistance = Mathf.Infinity)
		{
			;
		}
		public void WakeUp()
		{
			;
		}
		public abstract void OnCollisionEnter(Collision collision)
		{
			;
		}
		public abstract void OnCollisionStay(Collision collision)
		{
			;
		}
		public abstract void OnCollisionExit(Collision collision)
		{
			;
		}
	}

	public class Collider : MonoBehaviour
	{
		public RigidBody			attachedRigidBody;
		public Bounds				bounds;
		public float				contactOffset;
		public bool					enabled;
		public bool					isTrigger;
		public PhysicMaterial		material;
		public PhysicMaterial		sharedMaterial;
		private List<JVector>		vertices;
		private List<TriangleVertexIndices> = new List<TriangleVertexIndices>();
		public Vector3				ClosestPointOnBounds(Vector3 position)
		{
			float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));
		}

		public bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);
		}

		public Collider()
		{
			;
		}
		public abstract void OnCollisionEnter(Collision collision)
		{
			;
		}
		public abstract void OnCollisionStay(Collision collision)
		{
			;
		}
		public abstract void OnCollisionExit(Collision collision)
		{
			;
		}
		public abstract void OnTriggerEnter(Collision collision)
		{
			;
		}
		public abstract void OnTriggerStay(Collision collision)
		{
			;
		}
		public abstract void OnTriggerExit(Collision collision)
		{
			;
		}
		//rajouter tag a monobehaviour
	}
	public class BoxCollider : Collider
	{
		public Vector3				center;
		public Size					size;
		public Vector3				ClosestPointOnBounds(Vector3 worldPosition)
		{
			float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));
		}

		public bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);
		}
		public BoxCollider()
		{
			;
		}
	}

	public class SphereCollider : Collider
	{
		public override Vector3				ClosestPointOnBounds(Vector3 worldPosition)
		{
			float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));
		}
		public override bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);
		}
		public SphereCollider()
		{
			;
		}
	}
	public class MeshCollider : Collider
	{
		public override Vector3				ClosestPointOnBounds(Vector3 worldPosition)
		{
			float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));
		}
		public override bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);
		}
		public MeshCollider()
		{
			;
		}
	}
}